![nsu_logo](https://upload.wikimedia.org/wikipedia/commons/thumb/4/4e/NovosibirskStateUniversity_Logo_Eng.png/320px-NovosibirskStateUniversity_Logo_Eng.png)
# Прикладные алгоритмы обработки цифровых изображений
Выполнил: **[Неволин Владимир](https://vk.com/vatbon)** \
Электронная почта: **vatbon@gmail.com**\
Электронная почта(НГУ): **v.nevolin@g.nsu.ru**
## Лабораторная 3 (Морфология)
Реализованные фильтры в пределах данного задания
* [Расширение (Dilatation)](./src/main/java/vatbon/model/filter/morphology/DilatationFilter.java)
* [Эрозия (Erosion)](./src/main/java/vatbon/model/filter/morphology/ErosionFilter.java)
* [Заполнение дыр](./src/main/java/vatbon/model/filter/morphology/FillHolesFilter.java)
    * Алгоритм представляет собой заливку на основе расширения.
* [Обнаружение границ](./src/main/java/vatbon/model/filter/morphology/EdgeDetectionFilter.java)
* [Distance transform](./src/main/java/vatbon/model/filter/morphology/DistanceTransformFilter.java)
    * Алгоритм ищет ближайшую черную клетку, после чего окрашивает пиксель в оттенок белого. 
* [Подсчет клеток](./src/main/java/vatbon/model/filter/morphology/CountCellsFilter.java)
    * Фильтр после выполнения выделяет в прямоугольники найденные клетки. Красные клетки считаются как одинарные.
      Зеленые - обладающие большой удлиненностью, считаются как две.
      В стандартный поток вывода записываются результаты работы фильтра.
### Описание алгоритмов
#### Первый алгоритм
**Первый алгоритм обнаружения** построен на следующих фильтрах(в указанной последовательности):
* Фильтр Оцу
* Фильтр Собеля
* Расширение с примитивом
````
0 1 0
1 1 1
0 1 0 // Далее "крест"
````
* Эрозия с примитивом "крест"
* Обнаружение границ
* Заполнение дыр

**Первый алгоритм подсчета**  работает следующим образом(в указанной последовательности):
* Эрозия с примитивом 
````
0 0 0 0 1 1 1 0 0 0 0
0 0 1 1 1 1 1 1 1 0 0
0 1 1 1 1 1 1 1 1 1 0
0 1 1 1 1 1 1 1 1 1 0
1 1 1 1 1 1 1 1 1 1 1
1 1 1 1 1 1 1 1 1 1 1
1 1 1 1 1 1 1 1 1 1 1
0 1 1 1 1 1 1 1 1 1 0
0 1 1 1 1 1 1 1 1 1 0
0 0 1 1 1 1 1 1 1 0 0
0 0 0 0 1 1 1 0 0 0 0 // далее "большой круг"
````
* Эрозия с примитивом "большой круг"
* Эрозия с примитивом "большой круг"
* Эрозия с примитивом 
````
0 0 1 1 1 0 0
0 1 1 1 1 1 0
1 1 1 1 1 1 1
1 1 1 1 1 1 1
1 1 1 1 1 1 1
0 1 1 1 1 1 0
0 0 1 1 1 0 0 // далее "круг"
````
* Подсчет клеток с использованием соответствующего фильтра
#### Второй алгоритм
**Второй алгоритм обнаружения** построен на следующих фильтрах(в указанной последовательности):
* Фильтр резкости
* Фильтр Оцу
* Негатив
* Расширение с примитивом
````
1 1 1
1 1 1
1 1 1
````
* Расширение с примитивом "круг" (см. Первый алгоритм)
* Эрозия с примитивом "круг" (см. Первый алгоритм)

**Второй алгоритм подсчета** работает следующим образом(в указанной последовательности):
* [Distance transform](./src/main/java/vatbon/model/filter/morphology/DistanceTransformFilter.java) 
* Порог с уровнем 36 в пространстве RGB
* Подсчет клеток с использованием соответствующего фильтра
## Лабораторная 4 (Сегментация)
Реализованные фильтры в пределах данного задания

* [К-средних](./src/main/java/vatbon/model/filter/segmentation/KMeansFilter.java)

<details>
<summary><em>Click this to collapse/fold.</em> Результат работы алгоритма при 4 кластерах.</summary>

![k-means-4](https://i.imgur.com/KObQPPd.png)

</details>

* [Split-merge](./src/main/java/vatbon/model/filter/segmentation/SplitMergeFilter.java)

<details>
<summary><em>Click this to collapse/fold.</em> Результат работы алгоритма при значении схожести 500.</summary>

![merge-500](https://i.imgur.com/C4dsAhv.png)

</details>

* [Mean Shift](./src/main/java/vatbon/model/filter/segmentation/MeanShiftFilter.java)

<details>
<summary><em>Click this to collapse/fold.</em> 
Результат работы алгоритма при значении ограничении в 15 итераций, размером окна 100 и с 4 ядрами 
без выделения "сирот"(пиксели, которые не попали ни в одно из окон).</summary>

![mean-shift](https://i.imgur.com/ROhCTVj.png)

</details>

## Лабораторная 5 (Обнаружение границ)
В рамках этой лабораторной реализован следующий алгоритм
* [harris-filter](./src/main/java/vatbon/model/filter/corner/HarrisFilter.java)

<details>
<summary><em>Click this to collapse/fold.</em> Результат работы алгоритма.</summary>

![harris-image](https://i.imgur.com/jhGqRHE.png)

</details>

## Лабораторная 7 (Optical Flow)
Реализованный алгоритм в пределах данного задания:
* [Лукас Канаде(Lucas-Kanade)](./src/main/java/vatbon/model/algorithm/LucasKanade.java)
<details>
<summary><em>Click this to collapse/fold.</em> Результат работы алгоритма.</summary>

Первый кадр:
![frame1](https://i.imgur.com/N8KlhJc.png)

Второй кадр:
![frame2](https://i.imgur.com/jWv5ZAc.png)

Результат:
![res](https://i.imgur.com/4aeY94S.png)

</details>
