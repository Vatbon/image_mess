package vatbon;

import javafx.application.Application;
import javafx.stage.Stage;
import vatbon.model.Model;

public class AppMain extends Application {

    @Override
    public void start(Stage primaryStage) {
        Model.start();
    }

    public static void main(String[] args) {
        launch(args);
    }
}

