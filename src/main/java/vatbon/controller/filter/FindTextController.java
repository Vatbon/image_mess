package vatbon.controller.filter;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import vatbon.controller.WindowManager;
import vatbon.model.Model;
import vatbon.model.filter.FindTextFilter;

public class FindTextController implements FilterController {
    public Button addButton;
    public TextField brightnessField;

    @Override
    public void init(Stage stage) {
        addButton.setOnAction(event -> {
            int i = 0;
            if (!brightnessField.getText().isBlank()) {
                i = Integer.parseInt(brightnessField.getText());
            }
            Model.addFilter(new FindTextFilter(i));
            WindowManager.closeThis(stage);
            WindowManager.getController().changeHSV();
        });
    }
}
