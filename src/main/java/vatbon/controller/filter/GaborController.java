package vatbon.controller.filter;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import vatbon.controller.WindowManager;
import vatbon.model.Model;
import vatbon.model.filter.edge.GaborFilter;

public class GaborController implements FilterController {
    public TextField gammaField;
    public TextField lambdaFiled;
    public Button addButton;

    @Override
    public void init(Stage stage) {
        addButton.setOnAction(event -> {
            double gamma = 0.1;
            double lambda = 2;
            if (!gammaField.getText().isBlank()) {
                gamma = Double.parseDouble(gammaField.getText());
            }
            if (!lambdaFiled.getText().isBlank()) {
                lambda = Double.parseDouble(lambdaFiled.getText());
            }
            Model.addFilter(new GaborFilter(gamma, lambda));
            WindowManager.closeThis(stage);
            WindowManager.getController().changeHSV();
        });
    }
}
