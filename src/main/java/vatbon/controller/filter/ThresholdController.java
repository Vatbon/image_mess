package vatbon.controller.filter;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import vatbon.controller.WindowManager;
import vatbon.model.Model;
import vatbon.model.filter.ThresholdFilter;

public class ThresholdController implements FilterController {
    public TextField thresholdField;
    public Button addButton;

    @Override
    public void init(Stage stage) {
        addButton.setOnAction(event -> {
            int alpha = 127;
            if (!thresholdField.getText().isBlank()) {
                alpha = Integer.parseInt(thresholdField.getText());
            }
            Model.addFilter(new ThresholdFilter(alpha));
            WindowManager.closeThis(stage);
            WindowManager.getController().changeHSV();
        });
    }
}
