package vatbon.controller.filter;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import vatbon.controller.WindowManager;
import vatbon.model.Model;
import vatbon.model.filter.GaussianFilter;

public class GaussianController implements FilterController {
    public TextField sigmaField;
    public Button addButton;

    @Override
    public void init(Stage stage) {
        addButton.setOnAction(event -> {
            double sigma = 1;
            if (!sigmaField.getText().isBlank()) {
                sigma = Double.parseDouble(sigmaField.getText());
            }
            Model.addFilter(new GaussianFilter(sigma));
            WindowManager.closeThis(stage);
            WindowManager.getController().changeHSV();
        });
    }
}
