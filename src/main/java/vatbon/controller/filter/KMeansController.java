package vatbon.controller.filter;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import vatbon.controller.WindowManager;
import vatbon.model.Model;
import vatbon.model.filter.segmentation.KMeansFilter;

public class KMeansController implements FilterController {
    public Button addButton;
    public TextField clustersField;

    @Override
    public void init(Stage stage) {
        addButton.setOnAction(event -> {
            int i = 0;
            if (!clustersField.getText().isBlank()) {
                i = Integer.parseInt(clustersField.getText());
            }
            Model.addFilter(new KMeansFilter(i));
            WindowManager.closeThis(stage);
            WindowManager.getController().changeHSV();
        });
    }
}
