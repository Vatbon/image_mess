package vatbon.controller.filter;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import vatbon.controller.WindowManager;
import vatbon.model.Model;
import vatbon.model.filter.SharpnessFilter;

public class SharpnessController implements FilterController {
    public TextField alphaField;
    public TextField sigmaField;
    public Button addButton;

    @Override
    public void init(Stage stage) {
        addButton.setOnAction(event -> {
            double alpha = 0.5;
            double sigma = 1;
            if (!alphaField.getText().isBlank()) {
                alpha = Double.parseDouble(alphaField.getText());
            }
            if (!sigmaField.getText().isBlank()) {
                sigma = Double.parseDouble(sigmaField.getText());
            }
            Model.addFilter(new SharpnessFilter(alpha, sigma));
            WindowManager.closeThis(stage);
            WindowManager.getController().changeHSV();
        });
    }
}
