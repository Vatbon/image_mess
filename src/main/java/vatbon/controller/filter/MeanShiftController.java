package vatbon.controller.filter;

import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import vatbon.controller.WindowManager;
import vatbon.model.Model;
import vatbon.model.filter.segmentation.MeanShiftFilter;

public class MeanShiftController implements FilterController {
    public Button addButton;
    public TextField maxIterField;
    public TextField windowSizeField;
    public TextField numOfClustersField;
    public RadioButton markRadio;

    @Override
    public void init(Stage stage) {
        addButton.setOnAction(event -> {
            int iter = 10;
            int num = 8;
            double size = 25;
            if (!maxIterField.getText().isBlank()) {
                iter = Integer.parseInt(maxIterField.getText());
            }
            if (!numOfClustersField.getText().isBlank()) {
                num = Integer.parseInt(numOfClustersField.getText());
            }
            if (!windowSizeField.getText().isBlank()) {
                size = Double.parseDouble(windowSizeField.getText());
            }
            Model.addFilter(new MeanShiftFilter(num, iter, size, markRadio.isSelected()));
            WindowManager.closeThis(stage);
            WindowManager.getController().changeHSV();
        });
    }
}
