package vatbon.controller.filter;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import vatbon.controller.WindowManager;
import vatbon.model.Model;
import vatbon.model.filter.segmentation.SplitMergeFilter;

public class SplitMergeController implements FilterController {
    public Button addButton;
    public TextField homogeneityField;

    @Override
    public void init(Stage stage) {
        addButton.setOnAction(event -> {
            double hom = 127;
            if (!homogeneityField.getText().isBlank()) {
                hom = Double.parseDouble(homogeneityField.getText());
            }
            Model.addFilter(new SplitMergeFilter(hom));
            WindowManager.closeThis(stage);
            WindowManager.getController().changeHSV();
        });
    }
}
