package vatbon.controller.filter;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import vatbon.controller.WindowManager;
import vatbon.model.Model;
import vatbon.model.filter.edge.CannyFilter;

public class CannyController implements FilterController {
    public TextField strongField;
    public TextField weakField;
    public Button addButton;

    @Override
    public void init(Stage stage) {
        addButton.setOnAction(event -> {
            short strong = 128;
            short weak = 64;
            if (!strongField.getText().isBlank()) {
                strong = Short.parseShort(strongField.getText());
            }
            if (!weakField.getText().isBlank()) {
                weak = Short.parseShort(weakField.getText());
            }
            Model.addFilter(new CannyFilter(strong, weak));
            WindowManager.closeThis(stage);
            WindowManager.getController().changeHSV();
        });
    }
}
