package vatbon.controller;

import javafx.geometry.Insets;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import lombok.SneakyThrows;
import vatbon.model.Model;
import vatbon.model.cells.HSVCell;
import vatbon.model.cells.LABCell;
import vatbon.model.cells.RGBCell;
import vatbon.model.filter.Filter;

import java.util.List;

public class MainController {

    public MenuItem openMenu;
    public ImageView imageView;
    public Label redValueLabel;
    public Label greenValueLabel;
    public Label blueValueLabel;
    public Label mouseXLabel;
    public Label mouseYLabel;
    public Label sizeModifierLabel;
    public Rectangle colorRect;
    public Slider hueSlider;
    public Slider saturationSlider;
    public Slider valueSlider;
    public Label hueValueLabel;
    public Label saturationValueLabel;
    public Label valueValueLabel;
    public Button resetDefault;
    public RadioMenuItem lHistShow;
    public RadioMenuItem rgbHistShow;
    public VBox chartContainer;
    public Label hueLabel;
    public Label satLabel;
    public Label valLabel;
    public Label lLabel;
    public Label aLabel;
    public Label bLabel;
    public RadioMenuItem applyHsvRadio;
    public MenuItem brightnessMenuItem;
    public MenuItem grayscaleMenuItem;
    public MenuItem gaborMenuItem;
    public MenuItem gaussianMenuItem;
    public MenuItem otsuMenuItem;
    public MenuItem sharpnessMenuItem;
    public MenuItem negativeFilterItem;
    public MenuItem sobelMenuItem;
    public MenuItem thresholdMenuItem;
    public MenuItem cannyMenuItem;
    public MenuItem findTextMenuItem;
    public MenuItem findCellsItem;
    public MenuItem countCellsItem;
    public MenuItem resetCellsItem;
    public MenuItem findCellsItem1;
    public MenuItem countCellsItem1;
    public Button hideBar;
    public ScrollPane histPane;
    public MenuItem kMeansMenuItem;
    public MenuItem splitMergeMenuItem;
    public MenuItem meanShiftMenuItem;
    public MenuItem harrisFilterItem;
    public MenuItem lucasKanadeItem;
    public MenuItem saveAsItem;

    private AreaChart<Number, Number> lhist = null;
    private AreaChart<Number, Number> rgbhist = null;

    private double hue = 0;
    private double saturation = 0;
    private double value = 0;

    private Image image;
    int size = 10;
    private double height;
    private double width;
    private VBox vBoxFilters;
    private int mouseX = 0;
    private int mouseY = 0;

    public double getHue() {
        return this.hue;
    }

    public double getSaturation() {
        return this.saturation;
    }

    public double getValue() {
        return this.value;
    }

    public void setHue(double hue) {
        this.hue = hue;
    }

    public void setSaturation(double saturation) {
        this.saturation = saturation;
    }

    public void setValue(double value) {
        this.value = value;
    }

    @SneakyThrows
    public void init() {

        initHistograms();
        initSlidersAndHSV();
        initFilterQueue();

        mouseXLabel.setVisible(false);
        mouseYLabel.setVisible(false);
        colorRect.setVisible(false);

        redValueLabel.setVisible(false);
        greenValueLabel.setVisible(false);
        blueValueLabel.setVisible(false);

        hueLabel.setVisible(false);
        satLabel.setVisible(false);
        valLabel.setVisible(false);

        lLabel.setVisible(false);
        aLabel.setVisible(false);
        bLabel.setVisible(false);

        sizeModifierLabel.setText(String.valueOf(((float) size) / 10));

        imageView.setOnScroll(scrollEvent -> {
            final double deltaY = scrollEvent.getDeltaY();
            if (deltaY > 0) {
                size++;
            } else if (deltaY < 0) {
                size = size > 1 ? size - 1 : size;
            }
            imageView.setFitHeight(height * (((float) size) / 10));
            imageView.setFitWidth(width * (((float) size) / 10));
            sizeModifierLabel.setText(String.valueOf(((float) size) / 10));
        });
        imageView.setOnMousePressed(mouseEvent -> updatePixelInfo(mouseEvent.getX(), mouseEvent.getY(), true));

        hideBar.setOnAction(event -> {
            if (histPane.getPrefWidth() > 0) {
                histPane.setPrefWidth(0);
                histPane.setMinWidth(0);
                histPane.setVisible(false);
                hideBar.setText("<<<");
            } else {
                histPane.setPrefWidth(360);
                histPane.setMinWidth(360);
                histPane.setVisible(true);
                hideBar.setText(">>>");
            }
        });

        /*init menu*/

        saveAsItem.setOnAction(event -> Model.saveAs());

        brightnessMenuItem.setOnAction(event -> WindowManager.openFilterWindow(WindowManager.FILTER.BRIGHTNESS));

        cannyMenuItem.setOnAction(event -> WindowManager.openFilterWindow(WindowManager.FILTER.CANNY));

        findTextMenuItem.setOnAction(event -> WindowManager.openFilterWindow(WindowManager.FILTER.FIND_TEXT));

        grayscaleMenuItem.setOnAction(event -> WindowManager.openFilterWindow(WindowManager.FILTER.GRAYSCALE));

        gaborMenuItem.setOnAction(event -> WindowManager.openFilterWindow(WindowManager.FILTER.GABOR));

        gaussianMenuItem.setOnAction(event -> WindowManager.openFilterWindow(WindowManager.FILTER.GAUSSIAN));

        otsuMenuItem.setOnAction(event -> WindowManager.openFilterWindow(WindowManager.FILTER.OTSU));

        sharpnessMenuItem.setOnAction(event -> WindowManager.openFilterWindow(WindowManager.FILTER.SHARPNESS));

        sobelMenuItem.setOnAction(event -> WindowManager.openFilterWindow(WindowManager.FILTER.SOBEL));

        thresholdMenuItem.setOnAction(event -> WindowManager.openFilterWindow(WindowManager.FILTER.THRESHOLD));

        negativeFilterItem.setOnAction(event -> WindowManager.openFilterWindow(WindowManager.FILTER.NEGATIVE));

        kMeansMenuItem.setOnAction(event -> WindowManager.openFilterWindow(WindowManager.FILTER.K_MEANS));

        splitMergeMenuItem.setOnAction(event -> WindowManager.openFilterWindow(WindowManager.FILTER.SPLIT_MERGE));

        meanShiftMenuItem.setOnAction(event -> WindowManager.openFilterWindow(WindowManager.FILTER.MEAN_SHIFT));

        harrisFilterItem.setOnAction(event -> WindowManager.openFilterWindow(WindowManager.FILTER.HARRIS));
        /**/

        lucasKanadeItem.setOnAction(event -> WindowManager.lucasKanade());

        /*init cell detection*/
        findCellsItem.setOnAction(event -> this.findCells());

        findCellsItem1.setOnAction(event -> this.findCells1());

        countCellsItem1.setOnAction(event -> this.countCells1());

        countCellsItem.setOnAction(event -> this.countCells());

        resetCellsItem.setOnAction(event -> this.resetImage());
    }

    private void countCells1() {
        if (image != null) {
            int count = Model.countCells1();
            System.out.println("Amount of cells found : " + count);
            updateFilterQueue();
        }
        updatePixelInfo(mouseX, mouseY, false);
    }

    private void findCells1() {
        if (image != null) {
            Model.findCells1();
            updateFilterQueue();
        }
        updatePixelInfo(mouseX, mouseY, false);
    }

    private void resetImage() {
        if (image != null) {
            Model.resetImage();
            updateFilterQueue();
        }
        updatePixelInfo(mouseX, mouseY, false);
    }

    private void countCells() {
        if (image != null) {
            int count = Model.countCells();
            System.out.println("Amount of cells found : " + count);
            updateFilterQueue();
        }
        updatePixelInfo(mouseX, mouseY, false);
    }

    private void findCells() {
        if (image != null) {
            Model.findCells();
            updateFilterQueue();
        }
        updatePixelInfo(mouseX, mouseY, false);
    }

    private void initFilterQueue() {
        chartContainer.setMinWidth(320);
        chartContainer.setPrefWidth(320);

        ScrollPane scrollPaneFilters = new ScrollPane();
        scrollPaneFilters.setPrefSize(320, 480);

        vBoxFilters = new VBox();
        vBoxFilters.setPrefWidth(240);
        vBoxFilters.setFillWidth(true);

        scrollPaneFilters.setContent(vBoxFilters);
        scrollPaneFilters.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        scrollPaneFilters.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        scrollPaneFilters.setMaxHeight(480);
        Label filterLabel = new Label();
        filterLabel.setPadding(new Insets(5, 10, 5, 24));
        filterLabel.setText("Filter Queue");
        Font font = Font.font("System", 20);
        filterLabel.setFont(font);
        chartContainer.getChildren().add(filterLabel);
        chartContainer.getChildren().add(scrollPaneFilters);
    }

    private void initSlidersAndHSV() {
        applyHsvRadio.setOnAction(event -> {
            if (applyHsvRadio.isSelected()) {
                changeHSV();
            } else {
                disableHSV();
            }
        });

        hueValueLabel.setText(String.valueOf((int) hueSlider.getValue()));
        hueSlider.valueProperty().addListener((observableValue, oldValue, newValue) -> {
            hue = newValue.intValue();
            hueValueLabel.setText(String.valueOf(newValue.intValue()));
            if (applyHsvRadio.isSelected()) {
                changeHSV();
            }
        });

        saturationValueLabel.setText(String.valueOf((int) saturationSlider.getValue()));
        saturationSlider.valueProperty().addListener((observableValue, oldValue, newValue) -> {
            saturation = newValue.intValue();
            saturationValueLabel.setText(String.valueOf(newValue.intValue()));
            if (applyHsvRadio.isSelected()) {
                changeHSV();
            }
        });

        valueValueLabel.setText(String.valueOf((int) valueSlider.getValue()));
        valueSlider.valueProperty().addListener((observableValue, oldValue, newValue) -> {
            value = newValue.intValue();
            valueValueLabel.setText(String.valueOf(newValue.intValue()));
            if (applyHsvRadio.isSelected()) {
                changeHSV();
            }
        });
    }

    @SneakyThrows
    private void initHistograms() {
        if (rgbhist == null) {
            final NumberAxis xAxis = new NumberAxis(0, 100, 1);
            final NumberAxis yAxis = new NumberAxis();
            xAxis.setTickLabelsVisible(false);
            xAxis.setTickMarkVisible(false);
            xAxis.setMinorTickVisible(false);

            yAxis.setTickLabelsVisible(false);
            yAxis.setTickMarkVisible(false);
            yAxis.setMinorTickVisible(false);

            rgbhist = new AreaChart<>(xAxis, yAxis);
            rgbhist.setPrefSize(320, 240);
            rgbhist.setTitle("RGB histogram");
            rgbhist.setCreateSymbols(false);
            rgbhist.setAnimated(false);
            rgbhist.setVerticalGridLinesVisible(false);
            rgbhist.setHorizontalGridLinesVisible(false);
            rgbhist.setLegendVisible(false);
            rgbhist.getStylesheets().add(this.getClass().getResource("/styleRGBHist.css").toURI().toString());
            chartContainer.getChildren().add(rgbhist);

            if (rgbHistShow.isSelected()) {
                rgbhist.setPrefSize(320, 240);
                rgbhist.setTitle("RGB histogram");
                setRGBHist();
            } else {
                rgbhist.setPrefSize(0, 0);
                rgbhist.setMinSize(0, 0);
                rgbhist.setTitle("");
            }
        }

        if (lhist == null) {
            final NumberAxis xAxis = new NumberAxis(0, 100, 1);
            final NumberAxis yAxis = new NumberAxis();
            xAxis.setTickLabelsVisible(false);
            xAxis.setTickMarkVisible(false);
            xAxis.setMinorTickVisible(false);

            yAxis.setTickLabelsVisible(false);
            yAxis.setTickMarkVisible(false);
            yAxis.setMinorTickVisible(false);

            lhist = new AreaChart<>(xAxis, yAxis);
            lhist.setPrefSize(320, 240);
            lhist.setTitle("L-component histogram");
            lhist.setCreateSymbols(false);
            lhist.setAnimated(false);
            lhist.setVerticalGridLinesVisible(false);
            lhist.setHorizontalGridLinesVisible(false);
            lhist.setLegendVisible(false);
            lhist.getStylesheets().add(this.getClass().getResource("/styleLHist.css").toURI().toString());
            chartContainer.getChildren();
            chartContainer.getChildren().add(lhist);

            if (lHistShow.isSelected()) {
                lhist.setPrefSize(320, 240);
                lhist.setTitle("L-component histogram");
                setLHist();
            } else {
                lhist.setPrefSize(0, 0);
                lhist.setMinSize(0, 0);
                lhist.setTitle("");
            }
        }

        lHistShow.setOnAction(event -> {
            if (lhist != null) {
                if (lHistShow.isSelected()) {
                    lhist.setPrefSize(320, 240);
                    lhist.setTitle("L-component histogram");
                    setLHist();
                } else {
                    lhist.setPrefSize(0, 0);
                    lhist.setMinSize(0, 0);
                    lhist.setTitle("");
                }
            }
        });

        rgbHistShow.setOnAction(event -> {
            if (rgbhist != null) {
                if (rgbHistShow.isSelected()) {
                    rgbhist.setPrefSize(320, 240);
                    rgbhist.setTitle("RGB histogram");
                    setRGBHist();
                } else {
                    rgbhist.setPrefSize(0, 0);
                    rgbhist.setMinSize(0, 0);
                    rgbhist.setTitle("");
                }
            }
        });
    }

    private void updatePixelInfo(double x, double y, boolean calculatePos) {
        if (image != null) {
            if (calculatePos) {
                mouseX = (int) (x / (((float) size) / 10));
                mouseY = (int) (y / (((float) size) / 10));
            }

            mouseXLabel.setVisible(true);
            mouseYLabel.setVisible(true);
            colorRect.setVisible(true);

            redValueLabel.setVisible(true);
            greenValueLabel.setVisible(true);
            blueValueLabel.setVisible(true);

            hueLabel.setVisible(true);
            satLabel.setVisible(true);
            valLabel.setVisible(true);

            lLabel.setVisible(true);
            aLabel.setVisible(true);
            bLabel.setVisible(true);

            final Color color = image.getPixelReader().getColor(mouseX, mouseY);
            RGBCell rgbCell = new RGBCell();
            rgbCell.setRed((int) Math.round(color.getRed() * 255));
            rgbCell.setGreen((int) Math.round(color.getGreen() * 255));
            rgbCell.setBlue((int) Math.round(color.getBlue() * 255));
            HSVCell hsvCell = new HSVCell(rgbCell);
            LABCell labCell = new LABCell(rgbCell);

            mouseXLabel.setText(String.valueOf(mouseX));
            mouseYLabel.setText(String.valueOf(mouseY));
            colorRect.setFill(Color.color(color.getRed(),
                    color.getGreen(),
                    color.getBlue()));

            redValueLabel.setText(String.valueOf(Math.round(color.getRed() * 255)));
            greenValueLabel.setText(String.valueOf(Math.round(color.getGreen() * 255)));
            blueValueLabel.setText(String.valueOf(Math.round(color.getBlue() * 255)));

            hueLabel.setText(String.format("%.2f", hsvCell.getHue()));
            satLabel.setText(String.format("%.2f", hsvCell.getSaturation()));
            valLabel.setText(String.format("%.2f", hsvCell.getValue()));

            lLabel.setText(String.format("%.2f", labCell.getL()));
            aLabel.setText(String.format("%.2f", labCell.getA()));
            bLabel.setText(String.format("%.2f", labCell.getB()));
        }
    }

    private void disableHSV() {
        hue = 0;
        saturation = 0;
        value = 0;
        Model.updateImage();
    }

    private void setRGBHist() {
        if (image != null) {
            final List<List<RGBCell>> rgbMatrix = Model.getActiveImageEntity().getRGBMatrix();

            int[] rr = new int[256];
            int[] gg = new int[256];
            int[] bb = new int[256];

            rgbMatrix.forEach(rgbCells -> rgbCells.forEach(rgbCell -> {
                rr[rgbCell.getRed()]++;
                gg[rgbCell.getGreen()]++;
                bb[rgbCell.getBlue()]++;
            }));

            int maxr = 1;
            int maxg = 1;
            int maxb = 1;

            for (int j : rr) {
                if (j > maxr) {
                    maxr = j;
                }
            }
            for (int j : gg) {
                if (j > maxg) {
                    maxg = j;
                }
            }
            for (int j : bb) {
                if (j > maxb) {
                    maxb = j;
                }
            }

            XYChart.Series<Number, Number> seriesr = new XYChart.Series<>();
            XYChart.Series<Number, Number> seriesg = new XYChart.Series<>();
            XYChart.Series<Number, Number> seriesb = new XYChart.Series<>();

            for (int i = 0; i < rr.length; i++) {
                seriesr.getData().add(new XYChart.Data<>(i, rr[i] / (double) maxr));
                seriesg.getData().add(new XYChart.Data<>(i, gg[i] / (double) maxg));
                seriesb.getData().add(new XYChart.Data<>(i, bb[i] / (double) maxb));
            }

            rgbhist.getData().clear();
            rgbhist.getData().addAll(seriesr, seriesg, seriesb);
        }
    }

    public void openImage() {
        image = WindowManager.loadImage();
        if (image != null) {
            height = image.getHeight();
            width = image.getWidth();
            imageView.setFitHeight(height * (((float) size) / 10));
            imageView.setFitWidth(width * (((float) size) / 10));
            imageView.setImage(image);
        }
        if (lHistShow.isSelected()) {
            setLHist();
        }
        if (rgbHistShow.isSelected()) {
            setRGBHist();
        }
    }

    public void changeHSV() {
        updateFilterQueue();
        if (image != null) {
            image = Model.updateImage();
            if (lHistShow.isSelected()) {
                setLHist();
                setRGBHist();
            }
        }
        updatePixelInfo(mouseX, mouseY, false);
    }

    private void updateFilterQueue() {
        final List<Filter> filters = Model.getFilters();
        vBoxFilters.getChildren().clear();
        filters.forEach(filter -> {
            HBox hbox = new HBox();
            hbox.setMinSize(320, Region.USE_COMPUTED_SIZE);
            hbox.setPrefSize(Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE);
            hbox.setPadding(new Insets(2, 0, 0, 5));

            Label name = new Label(filter.getName());
            name.setPadding(new Insets(0, 0, 0, 10));
            HBox.setHgrow(name, Priority.ALWAYS);

            Button deleteButton = new Button("Delete");
            deleteButton.setMinWidth(68);
            HBox.setHgrow(deleteButton, Priority.ALWAYS);

            RadioButton visibleButton = new RadioButton("Visible");
            visibleButton.setMinWidth(68);
            HBox.setHgrow(visibleButton, Priority.ALWAYS);

            hbox.getChildren().addAll(name, deleteButton, visibleButton);
            deleteButton.setOnAction(event -> {
                Model.deleteFilter(filter);
                updateFilterQueue();
                if (filter.isVisible()) {
                    changeHSV();
                }
            });
            visibleButton.setSelected(filter.isVisible());
            visibleButton.setOnAction(event -> {
                filter.setVisible(visibleButton.isSelected());
                changeHSV();
            });
            vBoxFilters.getChildren().add(hbox);
        });
    }

    private void setLHist() {
        if (image != null) {
            final List<List<LABCell>> labMatrix = Model.getActiveImageEntity().getLABMatrixOnlyL();

            int[] ls = new int[101];

            labMatrix.forEach(labCells -> labCells.forEach(labCell -> ls[(int) labCell.getL()]++));

            XYChart.Series<Number, Number> series = new XYChart.Series<>();
            for (int i = 0; i < ls.length; i++) {
                series.getData().add(new XYChart.Data<>(i, ls[i]));
            }

            lhist.getData().clear();
            lhist.getData().add(series);
        }
    }

    public void resetHSV() {
        hueSlider.setValue(0.0);
        saturationSlider.setValue(0.0);
        valueSlider.setValue(0.0);
    }

    public void closeButton() {
        WindowManager.close();
    }
}
