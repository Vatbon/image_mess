package vatbon.controller;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import lombok.SneakyThrows;
import vatbon.controller.filter.FilterController;
import vatbon.model.Model;
import vatbon.model.filter.BWFilter;
import vatbon.model.filter.NegativeFilter;
import vatbon.model.filter.OtsuFilter;
import vatbon.model.filter.corner.HarrisFilter;
import vatbon.model.filter.edge.SobelFilter;

import java.net.URL;
import java.util.Map;

public class WindowManager {
    private static Stage activeStage;
    private static MainController controller;

    public enum FILTER {
        BRIGHTNESS,
        GRAYSCALE,
        GABOR,
        GAUSSIAN,
        OTSU,
        SHARPNESS,
        SOBEL,
        THRESHOLD,
        CANNY,
        NEGATIVE,
        FIND_TEXT,
        K_MEANS,
        SPLIT_MERGE,
        MEAN_SHIFT,
        HARRIS
    }

    private static final Map<FILTER, String> filterFxml = Map.ofEntries(
            Map.entry(FILTER.BRIGHTNESS, "/brightness.fxml"),
            Map.entry(FILTER.GRAYSCALE, ""),
            Map.entry(FILTER.GABOR, "/gabor.fxml"),
            Map.entry(FILTER.GAUSSIAN, "/gaussian.fxml"),
            Map.entry(FILTER.OTSU, ""),
            Map.entry(FILTER.SHARPNESS, "/sharpness.fxml"),
            Map.entry(FILTER.SOBEL, ""),
            Map.entry(FILTER.THRESHOLD, "/threshold.fxml"),
            Map.entry(FILTER.CANNY, "/canny.fxml"),
            Map.entry(FILTER.NEGATIVE, ""),
            Map.entry(FILTER.FIND_TEXT, "/findText.fxml"),
            Map.entry(FILTER.K_MEANS, "/k-means.fxml"),
            Map.entry(FILTER.SPLIT_MERGE, "/splitMerge.fxml"),
            Map.entry(FILTER.MEAN_SHIFT, "/shiftmeans.fxml"),
            Map.entry(FILTER.HARRIS, "")
    );

    public static MainController getController() {
        return controller;
    }

    @SneakyThrows
    public static void start() {
        FXMLLoader loader = new FXMLLoader();
        URL xmlUrl = Model.class.getResource("/main.fxml");
        loader.setLocation(xmlUrl);
        Parent root = loader.load();
        controller = loader.getController();
        controller.init();
        Scene scene = new Scene(root);

        activeStage = new Stage();
        activeStage.setTitle("Image Mess");
        activeStage.setScene(scene);
        activeStage.setMinWidth(800);
        activeStage.setMinHeight(512);
        activeStage.show();
    }

    public static void close() {
        activeStage.close();
    }

    public static Image loadImage() {
        return Model.loadImage();
    }

    public static void openFilterWindow(FILTER filter) {
        final String s = filterFxml.get(filter);
        if (s.isEmpty()) {
            addFilterAndUpdate(filter);
        } else {
            openFilterWindow(s);
        }
    }

    private static void addFilterAndUpdate(FILTER filter) {
        switch (filter) {
            case GRAYSCALE:
                Model.addFilter(new BWFilter());
                break;
            case OTSU:
                Model.addFilter(new OtsuFilter());
                break;
            case SOBEL:
                Model.addFilter(new SobelFilter());
                break;
            case NEGATIVE:
                Model.addFilter(new NegativeFilter());
                break;
            case HARRIS:
                Model.addFilter(new HarrisFilter());
                break;
        }
        controller.changeHSV();
    }

    public static void lucasKanade() {
        Model.lucasKanade();
    }

    @SneakyThrows
    private static void openFilterWindow(String path) {
        FXMLLoader loader = new FXMLLoader();
        URL xmlUrl = Model.class.getResource(path);
        loader.setLocation(xmlUrl);
        Parent root = loader.load();
        FilterController controller = loader.getController();
        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.setTitle("Image Mess");
        stage.setScene(scene);
        controller.init(stage);
        stage.show();
    }

    public static void closeThis(Stage stage) {
        if (stage != null) {
            stage.close();
        }
    }
}

