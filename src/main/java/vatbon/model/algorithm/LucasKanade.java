package vatbon.model.algorithm;

import javafx.util.Pair;
import vatbon.model.filter.BWFilter;
import vatbon.model.filter.RGBContainer;

public class LucasKanade {

    private static final int windowSize = 24;

    private static final int[][] matrixH = {{-1, 0, 1}, {-2, 0, 2}, {-1, 0, 1}};
    private static final int[][] matrixV = {{-1, -2, -1}, {0, 0, 0}, {1, 2, 1}};
    private double[][] matrix;

    private static class Vector {
        double x;
        double y;
    }

    private void initMatrix() {
        matrix = new double[windowSize][windowSize];
        final int rightBound = windowSize / 2;
        for (int y = -rightBound; y < rightBound; y++) {
            for (int x = -rightBound; x < rightBound; x++) {
                matrix[x + rightBound][y + rightBound] =
                        (1.0 / (Math.PI * 2)) * (Math.exp((-(x * x + y * y) / (double) (2))));
            }
        }
    }

    public RGBContainer process(RGBContainer first, RGBContainer second) {
        final BWFilter bwFilter = new BWFilter();
        final RGBContainer result = new RGBContainer(first.getWidth(), first.getWidth());
        first = bwFilter.filter(first);
        second = bwFilter.filter(second);

        initMatrix();
        final Vector[] compute = compute(first.getRed(),
                second.getRed(),
                first.getWidth(),
                first.getHeight(),
                windowSize);
        final short[] red = result.getRed();
        final short[] green = result.getGreen();
        final short[] blue = result.getBlue();
        double maxX = 0;
        double maxY = 0;
        double minX = 0;
        double minY = 0;
        for (Vector vector : compute) {
            if (vector != null) {
                double x = vector.x;
                double y = vector.y;
                if (x > maxX) {
                    maxX = x;
                }
                if (x < minX) {
                    minX = x;
                }
                if (y > maxY) {
                    maxY = y;
                }
                if (y < minY) {
                    minY = y;
                }
            }
        }
        maxX = maxX == 0 ? 1 : Math.abs(maxX);
        maxY = maxY == 0 ? 1 : Math.abs(maxY);
        minX = minX == 0 ? 1 : Math.abs(minX);
        minY = minY == 0 ? 1 : Math.abs(minY);
        for (int x = 0; x < first.getWidth(); x++) {
            for (int y = 0; y < first.getHeight(); y++) {
                final int pos = y * first.getWidth() + x;
                if (compute[pos] == null) {
                    green[0] = blue[0] = red[pos] = 0;
                } else {
                    double u = compute[pos].x * 10;
                    double v = compute[pos].y * 10;
                    if (u > 0) {
                        u /= maxX;
                    } else {
                        u /= minX;
                    }
                    if (v > 0) {
                        v /= maxY;
                    } else {
                        v /= minY;
                    }
//                    if (u > 0) {
//                        red[pos] = (short) Math.min(255, Math.max(u * 255, 0));
//                        if (v > 0) {
//                            blue[pos] = (short) Math.min(255, Math.max(v * 255, 0));
//                        } else {
//                            green[pos] = (short) Math.min(255, Math.max(-v * 255, 0));
//                        }
//                    } else {
//                        blue[pos] = (short) Math.min(255, Math.max(-u * 255, 0));
//                        if (v > 0) {
//                            red[pos] = (short) Math.min(255, Math.max(v * 255, 0));
//                        } else {
//                            green[pos] = (short) Math.min(255, Math.max(-v * 255, 0));
//                        }
//                    }
                    red[pos] = green[pos] = blue[pos] = (short) Math.min(255, Math.max(Math.sqrt(u * u + v * v) * 255, 0));
                }
            }
        }
        return result;
    }

    private Vector[] compute(short[] i1, short[] i2, int width, int height, int windowSize) {
        Vector[] vectors = new Vector[width * height];
        //считаем производную по времени
        double[] diffT = new double[i1.length];
        double[] diffX = new double[i1.length];
        double[] diffY = new double[i1.length];
        for (int i = 0; i < diffT.length; i++) {
            diffT[i] = (double) i1[i] - (double) i2[i];
        }
        //производные по горизонтали и вертикали
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                final Pair<Double, Double> pair = throughMatr(i1, width, height, x, y);
                diffX[y * width + x] = pair.getKey();
                diffY[y * width + x] = pair.getValue();
            }
        }
        //решаем уравнение для каждой точки в изображении
        for (int x = windowSize; x < width - windowSize; x++) {
            for (int y = windowSize; y < height - windowSize; y++) {
                double[] pair = lsm(diffX, diffY, diffT, width, height, x, y);
                final Vector vector = new Vector();
                vector.x = pair[0];
                vector.y = pair[1];
                vectors[y * width + x] = vector;
            }
        }


        return vectors;
    }

    private double[] lsm(double[] diffX, double[] diffY, double[] diffT, int width, int height, int x, int y) {
        double[][] a = new double[2][2];
        double[] b = new double[2];
        double[] v = new double[2];
        a[0][0] = 0;
        a[0][1] = 0;
        a[1][0] = 0;
        a[1][1] = 0;
        b[0] = 0;
        b[1] = 0;
        for (int i1 = windowSize / 2 - windowSize; i1 < windowSize - windowSize / 2; i1++) {
            for (int i2 = windowSize / 2 - windowSize; i2 < windowSize - windowSize / 2; i2++) {
                final int pos = (i1 + y) * width + i2 + x;
                a[0][0] += diffX[pos] * diffX[pos] * weight(i2, i1);
                a[0][1] += diffX[pos] * diffY[pos] * weight(i2, i1);
                a[1][0] += diffX[pos] * diffY[pos] * weight(i2, i1);
                a[1][1] += diffY[pos] * diffY[pos] * weight(i2, i1);
                b[0] += diffX[pos] * diffT[pos] * weight(i2, i1);
                b[1] += diffY[pos] * diffT[pos] * weight(i2, i1);
            }
        }
        //inverse
        double det = a[0][0] * a[1][1] - a[0][1] * a[1][0];
        double temp = a[1][1];
        a[1][1] = a[0][0];
        a[0][0] = temp;
        a[0][0] = a[0][0] / det;
        a[1][1] = a[1][1] / det;
        a[0][1] = -a[0][1] / det;
        a[1][0] = -a[1][0] / det;
        //result
        v[0] = a[0][0] * b[0] + a[0][1] * b[1];
        v[1] = a[1][0] * b[0] + a[1][1] * b[1];
        return v;
    }

    private double weight(int x, int y) {
        return matrix[x + windowSize / 2][y + windowSize / 2];
    }

    private Pair<Double, Double> throughMatr(short[] col, int width, int height, int x, int y) {
        double resV = 0;
        double resH = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                int relX = x + j - 1;
                int relY = y + i - 1;
                if (relX < 0) {
                    relX = 0;
                }
                if (relX >= width) {
                    relX = width - 1;
                }
                if (relY < 0) {
                    relY = 0;
                }
                if (relY >= height) {
                    relY = height - 1;
                }
                resV += col[relY * width + relX] * matrixV[i][j];
                resH += col[relY * width + relX] * matrixH[i][j];

            }
        }
        return new Pair<>(resH, resV);
    }

}
