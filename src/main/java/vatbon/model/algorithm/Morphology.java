package vatbon.model.algorithm;

import vatbon.model.filter.*;
import vatbon.model.filter.edge.SobelFilter;
import vatbon.model.filter.morphology.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Morphology {
    private static final short[][] CROSS = {
            {0, 1, 0},
            {1, 1, 1},
            {0, 1, 0}};
    private static final short[][] SQUARE = {
            {1, 1, 1},
            {1, 1, 1},
            {1, 1, 1}};
    private static final short[][] CIRCLE = {
            {0, 0, 1, 1, 1, 0, 0},
            {0, 1, 1, 1, 1, 1, 0},
            {1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1},
            {0, 1, 1, 1, 1, 1, 0},
            {0, 0, 1, 1, 1, 0, 0}};
    private static final short[][] BIG_CIRCLE = {
            {0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0},
            {0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0},
            {0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
            {0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
            {0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
            {0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
            {0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0},
            {0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0}};

    private static final List<Filter> CELL_FIND_FILTERS = new ArrayList<>();
    private static final List<Filter> CELL_FIND_FILTERS1 = new ArrayList<>();
    private static final List<Filter> CELL_COUNT_FILTERS = new ArrayList<>();
    private static final List<Filter> CELL_COUNT_FILTERS1 = new ArrayList<>();
    private static int[] intsCells;
    private static int[] intsCountCells;
    private static int countCells = -1;
    private static int[] intsCells1;
    private static int[] intsCountCells1;
    private static int countCells1 = -1;
    private static CountCellsFilter countCellsFilter;
    private static CountCellsFilter countCellsFilter1;
    private static RGBContainer providedContainer;

    private Morphology() {
    }

    private static int[] putThroughFilters(RGBContainer container, List<Filter> filters) {
        filters.forEach(filter -> filter.filter(container));
        int[] intArr = new int[container.getWidth() * container.getHeight()];
        container.fillArr(intArr, intArr.length);
        return intArr;
    }

    private static boolean sameAsPrevious(RGBContainer container) {
        if (providedContainer == null) {
            providedContainer = container;
            return false;
        }
        boolean same = Arrays.equals(container.getRed(), providedContainer.getRed()) &&
                Arrays.equals(container.getGreen(), providedContainer.getGreen()) &&
                Arrays.equals(container.getBlue(), providedContainer.getBlue()) &&
                container.getHeight() == providedContainer.getHeight() &&
                container.getWidth() == providedContainer.getWidth();
        if (same) {
            return true;
        } else {
            providedContainer = container;
            intsCells = null;
            intsCells1 = null;
            intsCountCells = null;
            intsCountCells1 = null;
            return false;
        }
    }

    public static int[] findCells(RGBContainer container) {
        if (sameAsPrevious(container) && intsCells != null) {
            return intsCells;
        } else {
            intsCountCells = null;
        }
        if (CELL_FIND_FILTERS.isEmpty()) {
            CELL_FIND_FILTERS.add(new OtsuFilter());
            CELL_FIND_FILTERS.add(new SobelFilter());
            CELL_FIND_FILTERS.add(new DilatationFilter(CROSS));
            CELL_FIND_FILTERS.add(new ErosionFilter(CROSS));
            CELL_FIND_FILTERS.add(new EdgeDetectionFilter(SQUARE));
            CELL_FIND_FILTERS.add(new FillHolesFilter(SQUARE));
        }
        final int[] ints = putThroughFilters(container, CELL_FIND_FILTERS);
        intsCells = Arrays.copyOf(ints, ints.length);
        return intsCells;
    }

    public static int[] countCells(RGBContainer container) {
        if (sameAsPrevious(container) && intsCountCells != null) {
            return intsCountCells;
        }
        if (CELL_COUNT_FILTERS.isEmpty()) {
            CELL_COUNT_FILTERS.add(new ErosionFilter(BIG_CIRCLE));
            CELL_COUNT_FILTERS.add(new ErosionFilter(BIG_CIRCLE));
            CELL_COUNT_FILTERS.add(new ErosionFilter(BIG_CIRCLE));
            CELL_COUNT_FILTERS.add(new ErosionFilter(CIRCLE));
            countCellsFilter = new CountCellsFilter(6);
            CELL_COUNT_FILTERS.add(countCellsFilter);
        }
        RGBContainer rgbContainer = new RGBContainer(container.getWidth(), container.getHeight(), findCells(container));
        intsCountCells = putThroughFilters(rgbContainer, CELL_COUNT_FILTERS);
        countCells = countCellsFilter.getCount();
        return intsCountCells;
    }

    public static int getCountCells(RGBContainer container) {
        if (countCells == -1) {
            countCells(container);
        }
        return countCells;
    }

    public static int[] findCells1(RGBContainer container) {
        if (sameAsPrevious(container) && intsCells1 != null) {
            return intsCells1;
        } else {
            intsCountCells1 = null;
        }
        if (CELL_FIND_FILTERS1.isEmpty()) {
            CELL_FIND_FILTERS1.add(new SharpnessFilter(2, 1.0));
            CELL_FIND_FILTERS1.add(new OtsuFilter());
            CELL_FIND_FILTERS1.add(new NegativeFilter());
            CELL_FIND_FILTERS1.add(new DilatationFilter(SQUARE));
            CELL_FIND_FILTERS1.add(new DilatationFilter(CIRCLE));
            CELL_FIND_FILTERS1.add(new ErosionFilter(CIRCLE));
        }
        final int[] ints = putThroughFilters(container, CELL_FIND_FILTERS1);
        intsCells1 = Arrays.copyOf(ints, ints.length);
        return intsCells1;
    }

    public static int[] countCells1(RGBContainer container) {
        if (sameAsPrevious(container) && intsCountCells1 != null) {
            return intsCountCells1;
        }
        if (CELL_COUNT_FILTERS1.isEmpty()) {
            CELL_COUNT_FILTERS1.add(new DistanceTransformFilter());
            CELL_COUNT_FILTERS1.add(new ThresholdFilter(36));
            countCellsFilter1 = new CountCellsFilter(10);
            CELL_COUNT_FILTERS1.add(countCellsFilter1);
        }
        RGBContainer rgbContainer = new RGBContainer(container.getWidth(), container.getHeight(), findCells1(container));
        intsCountCells1 = putThroughFilters(rgbContainer, CELL_COUNT_FILTERS1);
        countCells1 = countCellsFilter1.getCount();
        return intsCountCells1;
    }

    public static int getCountCells1(RGBContainer container) {
        if (countCells1 == -1) {
            countCells1(container);
        }
        return countCells1;
    }
}
