package vatbon.model;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.scene.image.PixelFormat;
import javafx.scene.image.WritableImage;
import javafx.stage.FileChooser;
import lombok.SneakyThrows;
import vatbon.controller.WindowManager;
import vatbon.model.algorithm.LucasKanade;
import vatbon.model.algorithm.Morphology;
import vatbon.model.cells.HSVCell;
import vatbon.model.cells.RGBCell;
import vatbon.model.filter.Filter;
import vatbon.model.filter.RGBContainer;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Model {
    private static final List<Filter> FILTERS = new ArrayList<>();
    private static RGBContainer originalImageContainer;
    private static ImageEntity originalImage;
    private static ImageEntity activeImageEntity;
    private static WritableImage activeImage;
    private static int[] intArr;


    private Model() {
    }

    @SneakyThrows
    public static void start() {
        WindowManager.start();
    }

    @SneakyThrows
    public static int[] filterRGB(ImageEntity imageEntity) {
        final double hue = WindowManager.getController().getHue();
        final double saturation = WindowManager.getController().getSaturation();
        final double value = WindowManager.getController().getValue();

        final List<List<HSVCell>> hsvMatrix = imageEntity.getHSVMatrix();
        hsvMatrix.forEach(hsvCells -> hsvCells.forEach(hsvCell -> {
                    if (hue != 0) {
                        double h = hsvCell.getHue() + hue;
                        h = h < 0 ? 360 + h : h % 360;
                        hsvCell.setHue((float) h);
                    }
                    if (saturation != 0) {
                        double s = hsvCell.getSaturation() * (1.0 + saturation / 100.0);
                        s = s > 0 ? Math.min(s, 1.0) : 0;
                        hsvCell.setSaturation((float) s);
                    }
                    if (value != 0) {
                        double v = hsvCell.getValue() * (1.0 + value / 100.0);
                        v = v > 0 ? Math.min(v, 1.0) : 0;
                        hsvCell.setValue((float) v);
                    }
                })
        );
        for (int i = 0; i < originalImage.getHeight(); i++) {
            for (int j = 0; j < originalImage.getWidth(); j++) {
                HSVCell hsvCell = hsvMatrix.get(i).get(j);
                RGBCell rgbCell = new RGBCell(hsvCell);
                intArr[(int) (i * originalImage.getWidth() + j)] = (0xff << 24) |
                        (rgbCell.getRed() << 16) |
                        (rgbCell.getGreen() << 8) |
                        (rgbCell.getBlue());
            }
        }

        return putThroughQueue(intArr);
    }

    private static int[] putThroughQueue(int[] intarr) {
        RGBContainer container = new RGBContainer((int) originalImage.getWidth(),
                (int) originalImage.getHeight(), intarr);
        FILTERS.stream().filter(Filter::isVisible).forEach(filter -> filter.filter(container));
        container.fillArr(intarr, intarr.length);
        return intarr;
    }

    @SneakyThrows
    public static WritableImage updateImage() {
        final int[] ints = filterRGB(originalImage);
        showImage(ints);
        activeImageEntity = new ImageEntity(activeImage);
        return activeImage;
    }

    private static void showImage(int[] argb) {
        activeImage.getPixelWriter().setPixels(0, 0,
                (int) originalImage.getWidth(), (int) originalImage.getHeight(),
                PixelFormat.getIntArgbInstance(), argb, 0, (int) (originalImage.getWidth()));
    }

    @SneakyThrows
    public static void saveAs() {
        FileChooser fileChooser = new FileChooser();
        final File file = fileChooser.showSaveDialog(null);
        if (file != null) {
            String name = file.getName();
            String extension = name.substring(1 + name.lastIndexOf(".")).toLowerCase();
            final boolean validFormat = Arrays.asList(ImageIO.getWriterFormatNames()).contains(extension);
            if (!validFormat) {
                extension = "png";
            }
            BufferedImage bufferedImage = SwingFXUtils.fromFXImage(activeImage, null);
            ImageIO.write(bufferedImage, extension, file);
        }
    }

    public static Image loadImage() {
        FileChooser fileChooser = new FileChooser();
        final File file = fileChooser.showOpenDialog(null);
        if (file != null) {
            originalImage = new ImageEntity(new Image(file.toURI().toString()));
            originalImageContainer = null;
            intArr = new int[(int) (originalImage.getHeight() * originalImage.getWidth())];
            activeImage = new WritableImage((int) originalImage.getWidth(), (int) originalImage.getHeight());
            activeImageEntity = new ImageEntity(activeImage);
            return updateImage();
        } else {
            return null;
        }
    }

    public static void addFilter(Filter filter) {
        synchronized (FILTERS) {
            FILTERS.add(filter);
        }
    }

    public static List<Filter> getFilters() {
        return FILTERS;
    }

    public static void deleteFilter(Filter filter) {
        synchronized (FILTERS) {
            FILTERS.remove(filter);
        }
    }

    public static WritableImage resetImage() {
        return updateImage();
    }

    public static ImageEntity getActiveImageEntity() {
        return activeImageEntity;
    }

    public static void lucasKanade() {
        FileChooser fileChooser = new FileChooser();
        final File file = fileChooser.showOpenDialog(null);
        if (file != null) {
            ImageEntity newImage = new ImageEntity(new Image(file.toURI().toString()));
            if (newImage.getWidth() != originalImage.getWidth() || newImage.getHeight() != originalImage.getHeight()) {
                return;
            }
            RGBContainer container1 = getRawContainer(originalImage);
            RGBContainer container2 = getRawContainer(newImage);
            final LucasKanade lucasKanade = new LucasKanade();
            final RGBContainer process = lucasKanade.process(container1, container2);
            int[] argb = new int[container1.getWidth() * container1.getHeight()];
            process.fillArr(argb, argb.length);
            activeImage.getPixelWriter().setPixels(0, 0,
                    (int) originalImage.getWidth(), (int) originalImage.getHeight(),
                    PixelFormat.getIntArgbInstance(), argb, 0, (int) (originalImage.getWidth()));
            activeImageEntity = new ImageEntity(activeImage);
        }
    }

    private static RGBContainer getRawContainer(ImageEntity image) {
        if (image == null) {
            return null;
        }
        if (image == originalImage && originalImageContainer != null) {
            return originalImageContainer;
        }
        int[] ints = new int[(int) (image.getHeight() * image.getWidth())];
        final List<List<RGBCell>> rgbMatrix1 = image.getRGBMatrix();
        for (int i = 0; i < image.getHeight(); i++) {
            for (int j = 0; j < image.getWidth(); j++) {
                final RGBCell rgbCell1 = rgbMatrix1.get(i).get(j);
                ints[(int) (i * image.getWidth() + j)] = (0xff << 24) |
                        (rgbCell1.getRed() << 16) |
                        (rgbCell1.getGreen() << 8) |
                        (rgbCell1.getBlue());
            }
        }
        final RGBContainer container = new RGBContainer((int) image.getWidth(), (int) image.getHeight(), ints);
        if (image == originalImage) {
            originalImageContainer = container;
        }
        return container;
    }

    public static void findCells() {
        final RGBContainer rawContainer = getRawContainer(originalImage);
        showImage(Morphology.findCells(rawContainer));
    }

    public static int countCells() {
        final RGBContainer rawContainer = getRawContainer(originalImage);
        showImage(Morphology.countCells(rawContainer));
        return Morphology.getCountCells(rawContainer);
    }

    public static int countCells1() {
        final RGBContainer rawContainer = getRawContainer(originalImage);
        showImage(Morphology.countCells1(rawContainer));
        return Morphology.getCountCells1(rawContainer);
    }

    public static void findCells1() {
        final RGBContainer rawContainer = getRawContainer(originalImage);
        showImage(Morphology.findCells1(rawContainer));
    }
}
