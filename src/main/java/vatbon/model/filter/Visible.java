package vatbon.model.filter;

public interface Visible {
    void setVisible(boolean b);

    boolean isVisible();
}
