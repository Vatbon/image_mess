package vatbon.model.filter;

public class BrightnessFilter extends Filter {
    private final short brightPower;
    private static final String NAME = "Brightness Filter";

    public BrightnessFilter(int b) {
        this.brightPower = (short) b;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public RGBContainer filter(RGBContainer container) {
        final short[] red = container.getRed();
        final short[] green = container.getGreen();
        final short[] blue = container.getBlue();
        for (int i = 0; i < red.length; i++) {
            red[i] = (short) (Math.max(Math.min(red[i] + brightPower, 255), 0));
            green[i] = (short) (Math.max(Math.min(green[i] + brightPower, 255), 0));
            blue[i] = (short) (Math.max(Math.min(blue[i] + brightPower, 255), 0));
        }
        return container;
    }
}
