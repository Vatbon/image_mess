package vatbon.model.filter;

public class OtsuFilter extends Filter {
    private static final String NAME = "Otsu Filter";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public RGBContainer filter(RGBContainer container) {
        new BWFilter().filter(container);
        final short[] red = container.getRed();
        final short[] green = container.getGreen();
        final short[] blue = container.getBlue();
        long[] hist = calculateHist(red);
        long intensitySum = calculateIntSum(red);

        int bestThreshold = 0;
        double v = 0.0;

        int count = 0;
        int intSum = 0;

        // Перебираем границу между классами
        // thresh < INTENSITY_LAYER_NUMBER - 1, т.к. при 255 в ноль уходит знаменатель внутри for
        for (int i = 0; i < 255; ++i) {
            count += hist[i];
            intSum += i * hist[i];

            double firstProb = count / (double) red.length;
            double secondProb = 1.0 - firstProb;

            double firstMean = intSum / (double) count;
            double secondMean = (intensitySum - intSum)
                    / (double) (red.length - count);

            double meanDiff = firstMean - secondMean;

            double sigma = firstProb * secondProb * meanDiff * meanDiff;

            if (sigma > v) {
                v = sigma;
                bestThreshold = i;
            }
        }

        for (int i = 0; i < red.length; i++) {
            if (red[i] <= bestThreshold) {
                red[i] = green[i] = blue[i] = 0;
            } else {
                red[i] = green[i] = blue[i] = 255;
            }
        }

        return container;
    }

    private long calculateIntSum(short[] red) {
        long sum = 0;
        for (int i = 0; i < red.length; i++) {
            sum += red[i];
        }
        return sum;
    }

    private long[] calculateHist(short[] red) {
        long[] hist = new long[256];

        for (int i = 0; i < red.length; i++) {
            hist[red[i]]++;
        }

        return hist;
    }
}
