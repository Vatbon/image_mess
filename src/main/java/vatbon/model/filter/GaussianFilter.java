package vatbon.model.filter;

import java.util.Arrays;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ForkJoinPool;

public class GaussianFilter extends Filter {
    private static final String NAME = "Gaussian Filter";
    private final int size;
    private final double sigma;
    private double[][] matrix;

    public GaussianFilter(double sigma) {
        this.sigma = sigma;
        this.size = (int) Math.round(6 * sigma);
        initMatrix();
    }

    public GaussianFilter(int size, double sigma) {
        this.sigma = sigma;
        this.size = size;
        initMatrix();
    }

    private void initMatrix() {
        matrix = new double[size][size];
        final double sigmaDoubledSqr = 2 * sigma * sigma;
        final int rightBound = size / 2;
        for (int y = -rightBound; y < rightBound; y++) {
            for (int x = -rightBound; x < rightBound; x++) {
                matrix[x + rightBound][y + rightBound] =
                        (1.0 / (Math.PI * sigmaDoubledSqr)) * (Math.exp((-(x * x + y * y) / (sigmaDoubledSqr))));
            }
        }
    }

    private double throughMatr(short[] col, int width, int height, int x, int y) {
        double res = 0;
        final int relShift = size / 2;
        final double center = col[y * width + x];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                int relX = x + j - relShift;
                int relY = y + i - relShift;
                if (relX < 0 || relX >= width || relY < 0 || relY >= height) {
                    res += center * matrix[j][i];
                } else {
                    res += col[relY * width + relX] * matrix[j][i];
                }
            }
        }
        return res;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public RGBContainer filter(RGBContainer container) {
        final short[] red = container.getRed();
        final short[] green = container.getGreen();
        final short[] blue = container.getBlue();
        final short[] redCopy = Arrays.copyOf(red, red.length);
        final short[] greenCopy = Arrays.copyOf(green, green.length);
        final short[] blueCopy = Arrays.copyOf(blue, blue.length);
        final int width = container.getWidth();
        final int height = container.getHeight();

        ForkJoinPool forkJoinPool = new ForkJoinPool(3);
        CountDownLatch latch = new CountDownLatch(3);

        forkJoinPool.submit(() -> {
            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    red[y * width + x] = (short) Math.max(Math.min(throughMatr(redCopy, width, height, x, y), 255), 0);
                }
            }
            latch.countDown();
        });

        forkJoinPool.submit(() -> {
            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    green[y * width + x] = (short) Math.max(Math.min(throughMatr(greenCopy, width, height, x, y), 255), 0);
                }
            }
            latch.countDown();
        });

        forkJoinPool.submit(() -> {
            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    blue[y * width + x] = (short) Math.max(Math.min(throughMatr(blueCopy, width, height, x, y), 255), 0);
                }
            }
            latch.countDown();
        });

        try {
            latch.await();
        } catch (InterruptedException ignored) {
        }
        return container;
    }
}
