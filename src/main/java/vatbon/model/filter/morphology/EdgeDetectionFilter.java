package vatbon.model.filter.morphology;

import vatbon.model.filter.Filter;
import vatbon.model.filter.RGBContainer;

import java.util.Arrays;

public class EdgeDetectionFilter extends Filter {

    private static final String NAME = "Dilatation Algorithm";
    private final short[][] pattern;

    public EdgeDetectionFilter(short[][] pattern) {
        this.pattern = Arrays.copyOf(pattern, pattern.length);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public RGBContainer filter(RGBContainer container) {
        final short[] redCopy = Arrays.copyOf(container.getRed(), container.getRed().length);
        new ErosionFilter(pattern).filter(container);

        final short[] red = container.getRed();
        final short[] green = container.getGreen();
        final short[] blue = container.getBlue();

        for (int i = 0; i < red.length; i++) {
            red[i] = green[i] = blue[i] = (short) (redCopy[i] - red[i]);
        }

        return container;
    }
}
