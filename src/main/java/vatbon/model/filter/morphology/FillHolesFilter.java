package vatbon.model.filter.morphology;

import javafx.util.Pair;
import vatbon.model.filter.Filter;
import vatbon.model.filter.NegativeFilter;
import vatbon.model.filter.RGBContainer;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Queue;

public class FillHolesFilter extends Filter {
    private static final String NAME = "Reconstruction delatation Algorithm";
    private final short[][] pattern;
    private final int size;
    private final Queue<Pair<Integer, Integer>> pixels = new LinkedList<>();
    private final Collection<Pair<Integer, Integer>> coll = new LinkedList<>();

    public FillHolesFilter(short[][] pattern) {
        this.pattern = Arrays.copyOf(pattern, pattern.length);
        this.size = pattern.length;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public RGBContainer filter(RGBContainer container) {
        final short[] red = container.getRed();
        final short[] green = container.getGreen();
        final short[] blue = container.getBlue();
        final int width = container.getWidth();
        final int height = container.getHeight();

        final short[] Ic = new short[red.length];
        final short[] res = new short[red.length];

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if ((y == 0 || y == height - 1) || (x == 0 || x == width - 1)) {
                    pixels.add(new Pair<>(x, y));
                }
                Ic[y * width + x] = (short) (red[y * width + x] > 0 ? 0 : 255);
            }
        }
        boolean st;
        do {
            st = step(res, Ic, width, height);
        } while (st);
        for (int i = 0; i < red.length; i++) {
            red[i] = blue[i] = green[i] = res[i];
        }
        new NegativeFilter().filter(container);
        return container;
    }

    private boolean step(short[] res, short[] mask, int width, int height) {
        final short[] beforeStep = Arrays.copyOf(res, res.length);
        boolean ch = false;
        delatation(res, mask, width, height);
        for (int i = 0; i < res.length; i++) {
            if (beforeStep[i] != res[i]) {
                ch = true;
                break;
            }
        }
        return ch;
    }

    private void delatation(short[] res, short[] mask, int width, int height) {
        while (!pixels.isEmpty()) {
            final Pair<Integer, Integer> poll = pixels.poll();
            throughMatr(res, mask, width, height, poll.getKey(), poll.getValue());
        }
        pixels.addAll(coll);
        coll.clear();
    }

    private void throughMatr(short[] res, short[] mask, int width, int height, int x, int y) {
        final int relShift = size / 2;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                int relX = x + j - relShift;
                int relY = y + i - relShift;
                if (relX < 0 || relX >= width || relY < 0 || relY >= height) {
                    continue;
                } else {
                    if (pattern[i][j] > 0 && res[relY * width + relX] == 0 && mask[relY * width + relX] > 0) {
                        res[relY * width + relX] = 255;
                        coll.add(new Pair<>(relX, relY));
                    }
                }
            }
        }
    }
}
