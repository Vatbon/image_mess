package vatbon.model.filter.morphology;

import javafx.util.Pair;
import vatbon.model.filter.Filter;
import vatbon.model.filter.RGBContainer;

import java.util.*;

public class CountCellsFilter extends Filter {
    private static final String NAME = "Counter Algorithm";
    private short[] orig;
    private static final short[][] PATTERN = {{1, 1, 1}, {1, 1, 1}, {1, 1, 1}};
    private final short[] randRed = new short[2500];
    private final short[] randGreen = new short[2500];
    private final short[] randBlue = new short[2500];
    private double s = 0;
    private double xSum = 0;
    private double ySum = 0;
    private final List<Pair<Integer, Integer>> dots = new ArrayList<>();
    private static final int SIZE = PATTERN.length;
    private int count = 0;

    private final Queue<Pair<Integer, Integer>> pixels = new LinkedList<>();
    private int mostLeft;
    private int mostDown;
    private int mostRight;
    private int mostUp;
    private final int elongationThreshold;

    public CountCellsFilter(int elongationThreshold) {
        this.elongationThreshold = elongationThreshold;
    }

    private static class Shape {
        int mostLeft;
        int mostDown;
        int mostRight;
        int mostUp;
        double elongation;
        double space;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public RGBContainer filter(RGBContainer container) {
        count = 0;
        short[] red = container.getRed();
        final short[] green = container.getGreen();
        final short[] blue = container.getBlue();
        orig = Arrays.copyOf(red, red.length);
        final int width = container.getWidth();
        final int height = container.getHeight();

        Random random = new Random();

        for (int i = 0; i < randRed.length; i++) {
            randRed[i] = (short) random.nextInt();
            randGreen[i] = (short) random.nextInt();
            randBlue[i] = (short) random.nextInt();
        }

        boolean[] flagged = new boolean[red.length];
        Arrays.fill(flagged, false);
        System.out.println("X   |Y   |Elongation");
        markNeighbours(red, green, blue, flagged, width, height);

        return container;
    }

    private void markNeighbours(short[] red, short[] green, short[] blue, boolean[] flagged, int width, int height) {
        List<Shape> shapes = new ArrayList<>();
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (red[y * width + x] > 0 && !flagged[y * width + x]) {
                    count++;
                    pixels.add(new Pair<>(x, y));
                    xSum = 0;
                    ySum = 0;
                    s = 0;
                    mostLeft = Integer.MAX_VALUE;
                    mostDown = Integer.MAX_VALUE;
                    mostRight = Integer.MIN_VALUE;
                    mostUp = Integer.MIN_VALUE;
                    dots.clear();
                    fillRegion(red, green, blue, flagged, orig, width, height);
                    double xCenter = xSum / s;
                    double yCenter = ySum / s;
                    double m02 = 0;
                    double m20 = 0;
                    double m11 = 0;
                    for (Pair<Integer, Integer> dot : dots) {
                        Integer xx = dot.getKey();
                        Integer yy = dot.getValue();
                        m20 += (xx - xCenter) * (xx - xCenter);
                        m02 += (yy - yCenter) * (yy - yCenter);
                        m11 += (xx - xCenter) * (yy - yCenter);
                    }
                    final double sqrt = Math.sqrt(Math.pow(m02 - m20, 2) + 4 * m11 * m11);
                    double elongation = (m02 + m20 + sqrt) / (m02 + m20 - sqrt);

                    Shape shape = new Shape();
                    shape.space = s;
                    shape.elongation = elongation;
                    shape.mostLeft = mostLeft;
                    shape.mostRight = mostRight;
                    shape.mostDown = mostDown;
                    shape.mostUp = mostUp;
                    shapes.add(shape);
                    System.out.printf("%-4d|%-4d|%.3f%n", Math.round(xCenter), Math.round(yCenter), elongation);
                }
            }
        }

        shapes.forEach(shape -> {
            int left = shape.mostLeft;
            int right = shape.mostRight;
            int down = shape.mostDown;
            int up = shape.mostUp;
            if (shape.elongation < elongationThreshold) {
                for (int xx = left; xx < right; xx++) {
                    red[down * width + xx] = 255;
                    red[up * width + xx] = 255;
                }
                for (int yy = down; yy < up; yy++) {
                    red[yy * width + left] = 255;
                    red[yy * width + right] = 255;
                }
            } else {
                for (int xx = left; xx < right; xx++) {
                    green[down * width + xx] = 255;
                    green[up * width + xx] = 255;
                }
                for (int yy = down; yy < up; yy++) {
                    green[yy * width + left] = 255;
                    green[yy * width + right] = 255;
                }
                count++;
            }
        });
        shapes.clear();
    }

    private void fillRegion(short[] red, short[] green, short[] blue, boolean[] flagged, short[] orig, int width, int height) {
        final int relShift = SIZE / 2;
        while (!pixels.isEmpty()) {
            Pair<Integer, Integer> poll = pixels.poll();
            Integer x = poll.getKey();
            Integer y = poll.getValue();
            for (int i = 0; i < SIZE; i++) {
                for (int j = 0; j < SIZE; j++) {
                    int relX = x + j - relShift;
                    int relY = y + i - relShift;
                    if (relX < 0 || relX >= width || relY < 0 || relY >= height) {
                        continue;
                    } else {
                        if (PATTERN[i][j] > 0 && orig[relY * width + relX] > 0 && !flagged[relY * width + relX]) {
                            if (relX < mostLeft) {
                                mostLeft = relX;
                            }
                            if (relX > mostRight) {
                                mostRight = relX;
                            }
                            if (relY < mostDown) {
                                mostDown = relY;
                            }
                            if (relY > mostUp) {
                                mostUp = relY;
                            }

                            red[relY * width + relX] = randRed[count];
                            green[relY * width + relX] = randGreen[count];
                            blue[relY * width + relX] = randBlue[count];

                            flagged[relY * width + relX] = true;

                            Pair<Integer, Integer> pair = new Pair<>(relX, relY);

                            s++;
                            xSum += relX;
                            ySum += relY;
                            pixels.add(pair);
                            dots.add(pair);
                        }
                    }
                }
            }
        }
    }

    public int getCount() {
        return count;
    }
}
