package vatbon.model.filter.morphology;

import vatbon.model.filter.Filter;
import vatbon.model.filter.RGBContainer;

public class DistanceTransformFilter extends Filter {

    @Override
    public String getName() {
        return "Distance Transform Filter";
    }

    @Override
    public RGBContainer filter(RGBContainer container) {
        final short[] red = container.getRed();
        final short[] redCopy = new short[red.length];
        final short[] green = container.getGreen();
        final short[] blue = container.getBlue();
        final int width = container.getWidth();
        final int height = container.getHeight();

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                redCopy[y * width + x] = findClosestBlack(red, width, height, x, y);
            }
        }
        for (int i = 0; i < redCopy.length; i++) {
            red[i] = green[i] = blue[i] = redCopy[i];
        }
        return container;
    }

    private short findClosestBlack(short[] red, int width, int height, int x, int y) {
        for (int dist = 0; dist < 255; dist++) {
            for (int _x = x - dist; _x < x + dist; _x++) {
                for (int _y = y - dist; _y < y + dist; _y++) {
                    if (!(_x < 0 || _x >= width || _y < 0 || _y >= height)) {
                        if (red[_y * width + _x] == 0) {
                            return (short) Math.round(Math.sqrt((x - _x) * (x - _x) + (y - _y) * (y - _y)));
                        }
                    }
                }
            }
        }
        return 255;
    }
}
