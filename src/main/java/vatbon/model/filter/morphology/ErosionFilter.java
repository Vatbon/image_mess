package vatbon.model.filter.morphology;

import vatbon.model.filter.Filter;
import vatbon.model.filter.RGBContainer;

import java.util.Arrays;

public class ErosionFilter extends Filter {

    private static final String NAME = "Erosion Algorithm";
    private final short[][] pattern;
    private final int size;

    public ErosionFilter(short[][] pattern) {
        this.pattern = Arrays.copyOf(pattern, pattern.length);
        this.size = pattern.length;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public RGBContainer filter(RGBContainer container) {
        final short[] red = container.getRed();
        final short[] green = container.getGreen();
        final short[] blue = container.getBlue();
        final int width = container.getWidth();
        final int height = container.getHeight();

        final short[] redCopy = Arrays.copyOf(red, red.length);

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                throughMatr(red, green, blue, redCopy, width, height, x, y);
            }
        }

        return container;
    }

    private void throughMatr(short[] red, short[] green, short[] blue, short[] col, int width, int height, int x, int y) {
        if (col[y * width + x] > 0) {
            boolean res = true;
            final int relShift = size / 2;
            for (int i = 0; i < size && res; i++) {
                for (int j = 0; j < size && res; j++) {
                    int relX = x + j - relShift;
                    int relY = y + i - relShift;
                    if (relX < 0 || relX >= width || relY < 0 || relY >= height) {
                        continue;
                    } else {
                        if (pattern[i][j] > 0 && col[relY * width + relX] == 0) {
                            res = false;
                        }
                    }
                }
            }
            if (res) {
                red[y * width + x] = green[y * width + x] = blue[y * width + x] = 255;
            } else {
                red[y * width + x] = green[y * width + x] = blue[y * width + x] = 0;
            }
        }
    }
}

