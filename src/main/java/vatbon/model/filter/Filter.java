package vatbon.model.filter;


public abstract class Filter implements Visible, Named {

    private boolean isVisible = true;

    public RGBContainer filter(RGBContainer container) {
        return container;
    }

    @Override
    public void setVisible(boolean visible) {
        isVisible = visible;
    }

    @Override
    public boolean isVisible() {
        return isVisible;
    }
}
