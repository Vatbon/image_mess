package vatbon.model.filter;

import java.util.Arrays;

public class SharpnessFilter extends Filter {
    private static final String NAME = "Sharpness Filter";
    private final double alpha;
    private final double sigma;

    public SharpnessFilter(double alpha, double sigma) {
        this.alpha = alpha;
        this.sigma = sigma;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public RGBContainer filter(RGBContainer container) {
        final short[] red = container.getRed();
        final short[] green = container.getGreen();
        final short[] blue = container.getBlue();
        final short[] redCopy = Arrays.copyOf(red, red.length);
        final short[] greenCopy = Arrays.copyOf(green, green.length);
        final short[] blueCopy = Arrays.copyOf(blue, blue.length);

        new GaussianFilter(sigma).filter(container);

        for (int i = 0; i < red.length; i++) {
            red[i] = (short) Math.max(Math.min(redCopy[i] + alpha * (redCopy[i] - red[i]), 255), 0);
            green[i] = (short) Math.max(Math.min(greenCopy[i] + alpha * (greenCopy[i] - green[i]), 255), 0);
            blue[i] = (short) Math.max(Math.min(blueCopy[i] + alpha * (blueCopy[i] - blue[i]), 255), 0);
        }

        return container;
    }
}
