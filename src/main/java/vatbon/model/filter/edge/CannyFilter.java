package vatbon.model.filter.edge;

import vatbon.model.filter.Filter;
import vatbon.model.filter.GaussianFilter;
import vatbon.model.filter.RGBContainer;

import java.util.Arrays;

public class CannyFilter extends Filter {

    private static final String NAME = "Canny Algorithm";
    private final short strongT;
    private final short weakT;

    public CannyFilter(short strongT, short weakT) {
        this.strongT = strongT;
        this.weakT = weakT;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public RGBContainer filter(RGBContainer container) {
        new GaussianFilter(5, 1).filter(container);
        new SobelFilter().filter(container);

        final short[] red = container.getRed();
        final short[] green = container.getGreen();
        final short[] blue = container.getBlue();
        final float[] direction = container.getDirection();
        final short[] redCopy = Arrays.copyOf(red, red.length);
        final short[] greenCopy = Arrays.copyOf(green, green.length);
        final short[] blueCopy = Arrays.copyOf(blue, blue.length);
        final int width = container.getWidth();
        final int height = container.getHeight();

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                greenCopy[y * width + x] =
                        suppressNonMax(redCopy, direction[y * width + x], x, y, width, height);
            }
        }

        markStrong(greenCopy, redCopy, blueCopy);
        short[] fin = linkWithStrong(redCopy, blueCopy, width, height);
        for (int i = 0; i < fin.length; i++) {
            red[i] = green[i] = blue[i] = fin[i];
        }

        return container;
    }


    private short[] linkWithStrong(short[] weak, short[] strong, int width, int height) {
        short[] res = new short[strong.length];
        for (int y = 1; y < height - 1; y++) {
            for (int x = 1; x < width - 1; x++) {
                if (strong[y * width + x] > 0) {
                    for (int i = y - 1; i < y + 1; i++) {
                        for (int j = x - 1; j < x + 1; j++) {
                            if (weak[i * width + j] > 0) {
                                res[i * width + j] = 255;
                            }
                        }
                    }
                }
            }
        }
        return res;
    }

    private void markStrong(short[] orig, short[] weak, short[] strong) {
        Arrays.fill(weak, (short) 0);
        Arrays.fill(strong, (short) 0);
        for (int i = 0; i < orig.length; i++) {
            if (orig[i] >= weakT) {
                weak[i] = 255;
            }
            if (orig[i] >= strongT) {
                strong[i] = 255;
            }
        }
    }

    private short suppressNonMax(short[] red, float v, int x, int y, int width, int height) {
        short c = red[y * width + x];
        if (Float.isNaN(v)) {
            v = 90;
        }
        if (x > 0 && x < width - 1 && y > 0 && y < height - 1) {
            if (67.5 <= v && v < 112.5) {
                short e = red[y * width + (x - 1)];
                short w = red[y * width + (x + 1)];
                if (!(c < e || c < w)) {
                    return c;
                }
            } else if (112.5 <= v && v < 157.5) {
                short ne = red[(y + 1) * width + (x - 1)];
                short sw = red[(y - 1) * width + (x + 1)];
                if (!(c < ne || c < sw)) {
                    return c;
                }
            } else if (v < 22.5 || v >= 157.5) {
                short s = red[(y - 1) * width + x];
                short n = red[(y + 1) * width + x];
                if (!(c < s || c < n)) {
                    return c;
                }
            } else {
                short se = red[(y - 1) * width + (x - 1)];
                short nw = red[(y + 1) * width + (x + 1)];
                if (!(c < se || c < nw)) {
                    return c;
                }
            }
        }
        return 0;
    }
}
