package vatbon.model.filter.edge;


import javafx.util.Pair;
import vatbon.model.filter.BWFilter;
import vatbon.model.filter.Filter;
import vatbon.model.filter.RGBContainer;

import java.util.Arrays;

public class SobelFilter extends Filter {
    private static final String NAME = "Sobel Filter";

    private static final int[][] matrixH = {{-1, 0, 1}, {-2, 0, 2}, {-1, 0, 1}};
    private static final int[][] matrixV = {{-1, -2, -1}, {0, 0, 0}, {1, 2, 1}};

    private Pair<Integer, Float> throughMatr(short[] col, int width, int height, int x, int y) {
        int resV = 0;
        int resH = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                int relX = x + j - 1;
                int relY = y + i - 1;
                if (relX < 0) {
                    relX = 0;
                }
                if (relX >= width) {
                    relX = width - 1;
                }
                if (relY < 0) {
                    relY = 0;
                }
                if (relY >= height) {
                    relY = height - 1;
                }
                resV += col[relY * width + relX] * matrixV[i][j];
                resH += col[relY * width + relX] * matrixH[i][j];

            }
        }
        double v = ((Math.atan(resV / (double) resH)) / Math.PI * 180) + 90;
        v = Math.abs(180 - v);
        return new Pair<>((int) Math.sqrt(resV * resV + resH * resH), (float) v);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public RGBContainer filter(RGBContainer container) {
        new BWFilter().filter(container);
        final short[] red = container.getRed();
        final short[] green = container.getGreen();
        final short[] blue = container.getBlue();
        final float[] direction = container.getDirection();
        final int width = container.getWidth();
        final int height = container.getHeight();

        final short[] redCopy = Arrays.copyOf(red, red.length);

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                final Pair<Integer, Float> pair = throughMatr(redCopy, width, height, x, y);
                red[y * width + x] = green[y * width + x] = blue[y * width + x] =
                        (short) Math.max(Math.min(pair.getKey(), 255), 0);
                direction[y * width + x] = pair.getValue();
            }
        }
        return container;
    }
}
