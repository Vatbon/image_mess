package vatbon.model.filter.edge;

import vatbon.model.filter.BWFilter;
import vatbon.model.filter.Filter;
import vatbon.model.filter.RGBContainer;

import java.util.Arrays;

public class GaborFilter extends Filter {
    private static final String NAME = "Gabor Filter";
    private final double gamma;
    private final double lambda;
    private final double phi = 0.0;
    private final double sigma;
    private final double[] theta = {0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170};
    private final int size;
    private double[][][] matrix;
    private final double gammaSqr;
    private final double dSigmaSqr;
    private final double dPiDivLambda;
    private boolean visible = true;

    public GaborFilter() {
        this.gamma = 0.1;
        this.lambda = 2;
        this.sigma = 0.56 * lambda;
        this.size = (int) Math.round(6 * sigma);
        this.gammaSqr = gamma * gamma;
        this.dSigmaSqr = 2 * sigma * sigma;
        this.dPiDivLambda = 2 * Math.PI / lambda;
        initMatrix();
    }

    public GaborFilter(double gamma, double lambda) {
        this.gamma = gamma;
        this.lambda = lambda;
        this.sigma = 0.56 * lambda;
        this.size = (int) Math.round(6 * sigma);
        this.gammaSqr = gamma * gamma;
        this.dSigmaSqr = 2 * sigma * sigma;
        this.dPiDivLambda = 2 * Math.PI / lambda;
        initMatrix();
    }

    private void initMatrix() {
        matrix = new double[theta.length][size][size];
        double xx;
        double yy;
        final int rightBound = size / 2;
        for (int i = 0; i < theta.length; i++) {
            for (int y = -rightBound; y < rightBound; y++) {
                for (int x = -rightBound; x < rightBound; x++) {
                    final double v = theta[i] * Math.PI / 180;
                    xx = x * Math.cos(v) + y * Math.sin(v);
                    yy = -x * Math.sin(v) + y * Math.cos(v);
                    matrix[i][x + rightBound][y + rightBound] = gabor(xx, yy);
                }
            }
        }
    }

    private double gabor(double xx, double yy) {
        return Math.exp(-(xx * xx + gammaSqr * yy * yy) / (dSigmaSqr)) * Math.cos(dPiDivLambda * xx + phi);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public RGBContainer filter(RGBContainer container) {
        new BWFilter().filter(container);
        final short[] red = container.getRed();
        final short[] green = container.getGreen();
        final short[] blue = container.getBlue();
        final int width = container.getWidth();
        final int height = container.getHeight();

        final short[] redCopy = Arrays.copyOf(red, red.length);
        final double[] fin = new double[red.length];

        double min = Double.MAX_VALUE;
        double max = Double.MIN_VALUE;

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                fin[y * width + x] = throughMatr(redCopy, width, height, x, y);
                if (fin[y * width + x] < min) {
                    min = fin[y * width + x];
                }
                if (fin[y * width + x] > max) {
                    max = fin[y * width + x];
                }
            }
        }
        min = Math.abs(min);
        max = 255.0 / (max + min);

        for (int i = 0; i < fin.length; i++) {
            red[i] = green[i] = blue[i] = (short) ((fin[i] + min) * max);
            if (red[i] > 255 || red[i] < 0) {
                System.out.println(red[i]);
            }
        }

        return container;
    }

    private double throughMatr(short[] col, int width, int height, int x, int y) {
        double res = 0;
        final int relShift = size / 2;
        final double center = col[y * width + x];
        for (int k = 0; k < theta.length; k++) {
            for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                    int relX = x + j - relShift;
                    int relY = y + i - relShift;
                    if (relX < 0 || relX >= width || relY < 0 || relY >= height) {
                        res += center * matrix[k][j][i];
                    } else {
                        res += col[relY * width + relX] * matrix[k][j][i];
                    }
                }
            }
        }
        return res / theta.length;
    }

    @Override
    public void setVisible(boolean b) {
        visible = b;
    }

    @Override
    public boolean isVisible() {
        return visible;
    }
}
