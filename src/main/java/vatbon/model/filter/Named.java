package vatbon.model.filter;

public interface Named {
    String getName();
}
