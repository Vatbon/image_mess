package vatbon.model.filter;

public class NegativeFilter extends Filter {
    private static final String NAME = "Negative Filter";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public RGBContainer filter(RGBContainer container) {
        final short[] red = container.getRed();
        final short[] green = container.getGreen();
        final short[] blue = container.getBlue();

        for (int i = 0; i < red.length; i++) {
            red[i] = (short) (255 - red[i]);
            green[i] = (short) (255 - green[i]);
            blue[i] = (short) (255 - blue[i]);
        }

        return container;
    }
}
