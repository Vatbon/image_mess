package vatbon.model.filter;

public class ThresholdFilter extends Filter {
    private static final String NAME = "Threshold Filter";
    private final int threshold;

    public ThresholdFilter(int threshold) {
        this.threshold = threshold;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public RGBContainer filter(RGBContainer container) {
        new BWFilter().filter(container);
        final short[] red = container.getRed();
        final short[] green = container.getGreen();
        final short[] blue = container.getBlue();

        for (int i = 0; i < red.length; i++) {
            if (red[i] <= threshold) {
                red[i] = green[i] = blue[i] = 0;
            } else {
                red[i] = green[i] = blue[i] = 255;
            }
        }
        return container;
    }
}
