package vatbon.model.filter;

import lombok.Getter;

@Getter
public class RGBContainer {
    private final int width;
    private final int height;
    private final short[] red;
    private final short[] green;
    private final short[] blue;
    private final float[] direction;

    public RGBContainer(int width, int height) {
        this.width = width;
        this.height = height;

        red = new short[width * height];
        green = new short[width * height];
        blue = new short[width * height];
        direction = new float[width * height];
    }

    public RGBContainer(int width, int height, final int[] arr) {
        this.width = width;
        this.height = height;

        red = new short[width * height];
        green = new short[width * height];
        blue = new short[width * height];
        direction = new float[width * height];

        for (int i = 0; i < arr.length; i++) {
            int a = arr[i]; //argb
            blue[i] = (short) (a & 0xff);
            green[i] = (short) (a >> 8 & 0xff);
            red[i] = (short) (a >> 16 & 0xff);
        }
    }

    public void fillArr(int[] intarr, int length) {
        for (int i = 0; i < length; i++) {
            intarr[i] = (0xff << 24) |
                    (red[i] << 16) |
                    (green[i] << 8) |
                    (blue[i]);
        }
    }
}
