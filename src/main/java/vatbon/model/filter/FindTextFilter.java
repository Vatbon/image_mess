package vatbon.model.filter;

import java.util.Arrays;

public class FindTextFilter extends Filter {
    private final int d;

    public FindTextFilter(int i) {
        d = i;
    }

    @Override
    public String getName() {
        return "Find Text Filter";
    }

    @Override
    public RGBContainer filter(RGBContainer container) {
        short[] red = container.getRed();
        short[] green = container.getGreen();
        short[] blue = container.getBlue();
        final short[] redCopy = Arrays.copyOf(red, red.length);
        final short[] greenCopy = Arrays.copyOf(green, green.length);
        final short[] blueCopy = Arrays.copyOf(blue, blue.length);

        new GaussianFilter(5, 1.0).filter(container);
        new NegativeFilter().filter(container);

        red = container.getRed();
        green = container.getGreen();
        blue = container.getBlue();

        for (int i = 0; i < redCopy.length; i++) {
            double dist = Math.sqrt((red[i] - redCopy[i]) * (red[i] - redCopy[i]) +
                    (green[i] - greenCopy[i]) * (green[i] - greenCopy[i]) +
                    (double) ((blue[i] - blueCopy[i]) * (blue[i] - blueCopy[i])));
            red[i] = dist <= d ? redCopy[i] : (short) Math.min(redCopy[i] * 2, 255);
            green[i] = dist <= d ? greenCopy[i] : (short) Math.min(greenCopy[i] * 2, 255);
            blue[i] = dist <= d ? blueCopy[i] : (short) Math.min(blueCopy[i] * 2, 255);
        }

        return container;
    }
}
