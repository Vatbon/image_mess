package vatbon.model.filter.segmentation;

import vatbon.model.filter.Filter;
import vatbon.model.filter.RGBContainer;

import java.util.Arrays;
import java.util.Random;

public class KMeansFilter extends Filter {
    private final int numOfClusters;
    private final Random random = new Random();

    private static class RGBPoint {
        short r;
        short g;
        short b;
        int mass;

        public RGBPoint(short r, short g, short b) {
            this.r = r;
            this.g = g;
            this.b = b;
            mass = 0;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            if (obj.getClass() == this.getClass()) {
                final RGBPoint point = (RGBPoint) obj;
                if (this.r == point.r && this.g == point.g && this.b == point.b) {
                    return true;
                }
            }
            return super.equals(obj);
        }
    }

    public KMeansFilter(int numOfClusters) {
        this.numOfClusters = numOfClusters;
    }

    @Override
    public String getName() {
        return "K-Means Filter";
    }

    @Override
    public RGBContainer filter(RGBContainer container) {
        final short[] red = container.getRed();
        final short[] green = container.getGreen();
        final short[] blue = container.getBlue();

        RGBPoint[] clusters = new RGBPoint[numOfClusters];


        for (int i = 0; i < numOfClusters; i++) {
            clusters[i] = new RGBPoint((short) Math.min(255, Math.max(0, random.nextInt())),
                    (short) Math.min(255, Math.max(0, random.nextInt())),
                    (short) Math.min(255, Math.max(0, random.nextInt())));
        }

        iterate(red, green, blue, clusters);

        for (int i = 0; i < red.length; i++) {
            RGBPoint closestCluster = null;
            double minDist = Double.MAX_VALUE;
            for (RGBPoint cluster : clusters) {
                final double dist = dist(cluster, red[i], green[i], blue[i]);
                if (minDist > dist) {
                    closestCluster = cluster;
                    minDist = dist;
                }
            }
            red[i] = closestCluster.r;
            green[i] = closestCluster.g;
            blue[i] = closestCluster.b;
        }

        return container;
    }

    private void iterate(short[] red, short[] green, short[] blue, RGBPoint[] clusters) {
        double[] reds = new double[numOfClusters];
        double[] greens = new double[numOfClusters];
        double[] blues = new double[numOfClusters];
        double[] masses = new double[numOfClusters];
        boolean isOn;

        do {
            isOn = false;
            Arrays.fill(reds, 0);
            Arrays.fill(blues, 0);
            Arrays.fill(greens, 0);
            Arrays.fill(masses, 0);
            for (int j = 0; j < red.length; j++) {
                double minDist = Double.MAX_VALUE;
                int minNum = 0;
                short r = red[j];
                short g = green[j];
                short b = blue[j];

                for (int i = 0; i < clusters.length; i++) {
                    final double dist = dist(clusters[i], r, g, b);
                    if (minDist > dist) {
                        minDist = dist;
                        minNum = i;
                    }
                }
                reds[minNum] += r;
                greens[minNum] += g;
                blues[minNum] += b;
                masses[minNum]++;
            }
            for (int i = 0; i < clusters.length; i++) {
                short newRed = (short) Math.round(reds[i] / masses[i]);
                short newGreen = (short) Math.round(greens[i] / masses[i]);
                short newBlue = (short) Math.round(blues[i] / masses[i]);
                final RGBPoint cluster = clusters[i];
                if (newRed != cluster.r || newGreen != cluster.g || newBlue != cluster.b) {
                    isOn = true;
                    cluster.r = newRed;
                    cluster.g = newGreen;
                    cluster.b = newBlue;
                }
            }
        } while (isOn);
    }

    private double dist(RGBPoint p1, short r, short g, short b) {
        return (double) (p1.r - r) * (p1.r - r) +
                (double) (p1.g - g) * (p1.g - g) +
                (double) (p1.b - b) * (p1.b - b);
    }
}
