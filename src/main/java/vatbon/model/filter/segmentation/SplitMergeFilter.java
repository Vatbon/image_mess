package vatbon.model.filter.segmentation;

import vatbon.model.filter.Filter;
import vatbon.model.filter.RGBContainer;

import java.util.LinkedList;
import java.util.Queue;

public class SplitMergeFilter extends Filter {
    private final double param;

    public SplitMergeFilter(double param) {
        this.param = param;
    }

    private static class Node {
        final int x;
        final int y;
        final int width;
        final int height;
        Node[] children = null;

        Node(int x, int y, int w, int h) {
            this.x = x;
            this.y = y;
            this.width = w;
            this.height = h;
        }
    }

    @Override
    public String getName() {
        return "Split&Merge Filter";
    }

    @Override
    public RGBContainer filter(RGBContainer container) {
        final int width = container.getWidth();
        final int height = container.getHeight();
        Node parent = new Node(0, 0, width, height);
        separate(parent, container);
        fillNodes(parent, container);
        return container;
    }

    private void fillNodes(Node node, RGBContainer container) {
        if (node.children == null) {
            final int hMax = node.y + node.height;
            final int hMin = node.y;
            final int wMax = node.x + node.width;
            final int wMin = node.x;
            double redMean = 0;
            double greenMean = 0;
            double blueMean = 0;
            final short[] red = container.getRed();
            final short[] green = container.getGreen();
            final short[] blue = container.getBlue();
            final int width = container.getWidth();
            for (int y = hMin; y < hMax; y++) {
                for (int x = wMin; x < wMax; x++) {
                    redMean += red[y * width + x];
                    greenMean += green[y * width + x];
                    blueMean += blue[y * width + x];
                }
            }
            redMean /= node.width * node.height;
            greenMean /= node.width * node.height;
            blueMean /= node.width * node.height;
            for (int y = hMin; y < hMax; y++) {
                for (int x = wMin; x < wMax; x++) {
                    red[y * width + x] = (short) redMean;
                    green[y * width + x] = (short) greenMean;
                    blue[y * width + x] = (short) blueMean;
                }
            }
        } else {
            for (Node child : node.children) {
                fillNodes(child, container);
            }
        }
    }

    private void separate(Node node, RGBContainer container) {
        Queue<Node> toSeparate = new LinkedList<>();
        toSeparate.add(node);
        while (!toSeparate.isEmpty()) {
            Node poll = toSeparate.poll();
            if (!isHomogeneous(poll, container)) {
                Node node1 = new Node(poll.x, poll.y,
                        poll.width / 2, poll.height / 2);
                Node node2 = new Node(poll.x + poll.width / 2, poll.y,
                        poll.width - poll.width / 2, poll.height / 2);
                Node node3 = new Node(poll.x, poll.y + poll.height / 2,
                        poll.width / 2, poll.height - poll.height / 2);
                Node node4 = new Node(poll.x + poll.width / 2, poll.y + poll.height / 2,
                        poll.width - poll.width / 2, poll.height - poll.height / 2);
                poll.children = new Node[]{node1, node2, node3, node4};
                toSeparate.add(node1);
                toSeparate.add(node2);
                toSeparate.add(node3);
                toSeparate.add(node4);
            }
        }
    }

    private boolean isHomogeneous(Node node, RGBContainer container) {
        if (Math.max(node.height, node.width) < 4) {
            return true;
        }
        final int hMax = node.y + node.height;
        final int hMin = node.y;
        final int wMax = node.x + node.width;
        final int wMin = node.x;
        double grayMean = 0;
        double sigma = 0;
        final short[] red = container.getRed();
        final short[] green = container.getGreen();
        final short[] blue = container.getBlue();
        final int width = container.getWidth();
        for (int y = hMin; y < hMax; y++) {
            for (int x = wMin; x < wMax; x++) {
                grayMean += (red[y * width + x] * 0.2126
                        + green[y * width + x] * 0.7152
                        + blue[y * width + x] * 0.0722);
            }
        }
        grayMean /= node.width * node.height;
        for (int y = hMin; y < hMax; y++) {
            for (int x = wMin; x < wMax; x++) {
                final double v = (red[y * width + x] * 0.2126
                        + green[y * width + x] * 0.7152
                        + blue[y * width + x] * 0.0722) - grayMean;
                sigma += v * v;
            }
        }
        sigma /= node.width * node.height - 1;
        return sigma < param;
    }
}
