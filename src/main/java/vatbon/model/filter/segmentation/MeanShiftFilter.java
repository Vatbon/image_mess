package vatbon.model.filter.segmentation;

import vatbon.model.filter.Filter;
import vatbon.model.filter.RGBContainer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

public class MeanShiftFilter extends Filter {
    private final int numOfClusters;
    private final int maxIterations;
    private final double windowSize;
    private final boolean markOrphans;
    private Random random;

    public MeanShiftFilter(int numOfClusters, int maxIterations, double windowSize, boolean markOrphans) {
        this.numOfClusters = numOfClusters;
        this.maxIterations = maxIterations;
        this.windowSize = windowSize;
        this.markOrphans = markOrphans;
    }

    private static class Kernel {
        short red;
        short green;
        short blue;
        double windowSize;

        Kernel(short red, short green, short blue, double windowSize) {
            this.red = red;
            this.green = green;
            this.blue = blue;
            this.windowSize = windowSize;
        }

        public boolean inWindow(short r, short g, short b) {
            return this.windowSize * this.windowSize > dist(r, g, b, this.red, this.green, this.blue);
        }

        private double dist(short r1, short g1, short b1, short r, short g, short b) {
            return (double) (r1 - r) * (r1 - r) +
                    (double) (g1 - g) * (g1 - g) +
                    (double) (b1 - b) * (b1 - b);
        }
    }

    @Override
    public String getName() {
        return "Mean Shift filter";
    }

    @Override
    public RGBContainer filter(RGBContainer container) {
        final short[] red = container.getRed();
        final short[] green = container.getGreen();
        final short[] blue = container.getBlue();
        final boolean[] orphans = new boolean[red.length];
        Arrays.fill(orphans, true);
        random = new Random(0);
        List<Kernel> kernels = new ArrayList<>();
        for (int i = 0; i < numOfClusters && hasOrphans(orphans); i++) {
            kernels.add(getNewCluster(red, green, blue, orphans));
        }
        for (int i = 0; i < red.length; i++) {
            Kernel closestKernel = kernels.get(0);
            double minDist = Double.MAX_VALUE;
            for (Kernel kernel : kernels) {
                final double dist = kernel.dist(kernel.red, kernel.green, kernel.blue, red[i], green[i], blue[i]);
                if (minDist > dist) {
                    closestKernel = kernel;
                    minDist = dist;
                }
            }
            red[i] = closestKernel.red;
            green[i] = closestKernel.green;
            blue[i] = closestKernel.blue;
        }
        if (markOrphans) {
            for (int i = 0; i < red.length; i++) {
                if (orphans[i]) {
                    red[i] = 255;
                    green[i] = 255;
                    blue[i] = 255;
                }
            }
        }
        return container;
    }

    private Kernel getNewCluster(short[] red, short[] green, short[] blue, boolean[] orphans) {
        final Kernel kernel = getNewKernel(red, green, blue, orphans);
        int i = 0;
        boolean hasChanged;
        do {
            hasChanged = iterate(red, green, blue, orphans, kernel);
            i++;
        } while (hasChanged && i < maxIterations);
        for (int k = 0; k < red.length; k++) {
            if (kernel.inWindow(red[k], green[k], blue[k])) {
                orphans[k] = false;
            }
        }
        return kernel;
    }

    private boolean iterate(short[] red, short[] green, short[] blue, boolean[] orphans, Kernel kernel) {
        boolean hasChanged = false;
        int redSum = 0;
        int greenSum = 0;
        int blueSum = 0;
        int amount = 0;
        for (int i = 0; i < red.length; i++) {
            final short r = red[i];
            final short g = green[i];
            final short b = blue[i];
            if (kernel.inWindow(r, g, b) && orphans[i]) {
                redSum += r;
                greenSum += g;
                blueSum += b;
                amount++;
            }
        }
        if (amount == 0) {
            return false;
        }
        short newRedCenter = (short) Math.round((double) redSum / (double) amount);
        short newGreenCenter = (short) Math.round((double) greenSum / (double) amount);
        short newBlueCenter = (short) Math.round((double) blueSum / (double) amount);
        if (kernel.red != newRedCenter || kernel.green != newGreenCenter || kernel.blue != newBlueCenter) {
            hasChanged = true;
            kernel.red = newRedCenter;
            kernel.green = newGreenCenter;
            kernel.blue = newBlueCenter;
        }
        return hasChanged;
    }

    private Kernel getNewKernel(short[] red, short[] green, short[] blue, boolean[] orphans) {
        final int size = (int) IntStream.range(0, orphans.length)
                .mapToObj(idx -> orphans[idx])
                .filter(Boolean::booleanValue).count();
        final int i = random.nextInt(size);
        return new Kernel(red[i], green[i], blue[i], this.windowSize);
    }

    private boolean hasOrphans(boolean[] orphans) {
        for (boolean b : orphans) {
            if (b) {
                return true;
            }
        }
        return false;
    }
}
