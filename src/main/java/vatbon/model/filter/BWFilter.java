package vatbon.model.filter;

public class BWFilter extends Filter {

    private static final String NAME = "B&W Filter";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public RGBContainer filter(RGBContainer container) {
        final short[] red = container.getRed();
        final short[] green = container.getGreen();
        final short[] blue = container.getBlue();

        for (int i = 0; i < red.length; i++) {
            red[i] = blue[i] = green[i] = (short) (red[i] * 0.2126 + green[i] * 0.7152 + blue[i] * 0.0722);
        }

        return container;
    }
}
