package vatbon.model.filter.corner;

import lombok.Getter;
import vatbon.model.filter.Filter;
import vatbon.model.filter.RGBContainer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class HarrisFilter extends Filter {
    private float[][] Lx2, Ly2, Lxy;
    private int width;
    private int height;
    private short[] pixel;

    private short getPixel(int x, int y) {
        if (x < 0) x = 0;
        if (y < 0) y = 0;
        if (x >= width) x = width - 1;
        if (y >= height) y = height - 1;
        return pixel[x + y * width];
    }

    private double gaussian(double x, double y, double sigma) {
        double sigma2 = sigma * sigma;
        double t = (x * x + y * y) / (2 * sigma2);
        double u = 1.0 / (2 * Math.PI * sigma2);
        return u * Math.exp(-t);
    }

    private float[] sobel(int x, int y) {
        int v00 = getPixel(x - 1, y - 1);
        int v10 = getPixel(x, y - 1);
        int v20 = getPixel(x + 1, y - 1);
        int v01 = getPixel(x - 1, y);
        int v21 = getPixel(x + 1, y);
        int v02 = getPixel(x - 1, y + 1);
        int v12 = getPixel(x, y + 1);
        int v22 = getPixel(x + 1, y + 1);
        float sx = ((v20 + 2 * v21 + v22) - (v00 + 2 * v01 + v02)) / (4 * 255f);
        float sy = ((v02 + 2 * v12 + v22) - (v00 + 2 * v10 + v20)) / (4 * 255f);
        return new float[]{sx, sy};
    }

    private void computeDerivatives(double sigma) {
        this.Lx2 = new float[width][height];
        this.Ly2 = new float[width][height];
        this.Lxy = new float[width][height];
        float[][][] grad = new float[width][height][];
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                grad[x][y] = sobel(x, y);
            }
        }
        int radius = (int) (2 * sigma);
        int window = 1 + 2 * radius;
        float[][] gaussian = new float[window][window];
        for (int j = -radius; j <= radius; j++) {
            for (int i = -radius; i <= radius; i++) {
                gaussian[i + radius][j + radius] = (float) gaussian(i, j, sigma);
            }
        }
        // Ix2 = (F) * (Gx^2)
        // Iy2 = (F) * (Gy^2)
        // Ixy = (F) * (Gx.Gy)
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                for (int dy = -radius; dy <= radius; dy++) {
                    for (int dx = -radius; dx <= radius; dx++) {
                        int xk = x + dx;
                        int yk = y + dy;
                        if (xk < 0 || xk >= width || yk < 0 || yk >= height) {
                            continue;
                        }
                        double gw = gaussian[dx + radius][dy + radius];
                        this.Lx2[x][y] += gw * grad[xk][yk][0] * grad[xk][yk][0];
                        this.Ly2[x][y] += gw * grad[xk][yk][1] * grad[xk][yk][1];
                        this.Lxy[x][y] += gw * grad[xk][yk][0] * grad[xk][yk][1];
                    }
                }
            }
        }
    }

    private float harrisMeasure(int x, int y, float k) {
        float m00 = this.Lx2[x][y];
        float m01 = this.Lxy[x][y];
        float m10 = this.Lxy[x][y];
        float m11 = this.Ly2[x][y];

        return m00 * m11 - m01 * m10 - k * (m00 + m11) * (m00 + m11);
    }

    private boolean isLocalMax(float[][] map, int x, int y) {
        int[] dx = new int[]{-1, 0, 1, 1, 1, 0, -1, -1};
        int[] dy = new int[]{-1, -1, -1, 0, 1, 1, 1, 0};
        for (int i = 0; i < dx.length; i++) {
            double wk = map[x + dx[i]][y + dy[i]];
            if (wk >= map[x][y]) {
                return false;
            }
        }
        return true;
    }

    private float[][] harrisMap(double k) {
        float[][] harrisMap = new float[width][height];
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                double h = harrisMeasure(x, y, (float) k);
                if (h <= 0) continue;
                h = 255 * Math.log(1 + h) / Math.log(1 + 255);
                harrisMap[x][y] = (float) h;
            }
        }

        return harrisMap;
    }

    private List<Point> detect(double sigma, double k) {
        computeDerivatives(sigma);
        float[][] harrisMap = harrisMap(k);
        ArrayList<Point> corners = new ArrayList<>();
        for (int y = 1; y < height - 1; y++) {
            for (int x = 1; x < width - 1; x++) {
                float h = harrisMap[x][y];
                if (h <= 0.001) continue;
                if (!isLocalMax(harrisMap, x, y)) continue;
                corners.add(new Point(x, y, h));
            }
        }

        Iterator<Point> iter = corners.iterator();
        while (iter.hasNext()) {
            Point p = iter.next();
            for (Point n : corners) {
                if (n == p) {
                    continue;
                }
                int dist = (p.getX() - n.getX()) * (p.getX() - n.getX()) + (p.getY() - n.getY()) * (p.getY() - n.getY());
                if (dist > 3 * 3 || n.getValue() < p.getValue()) {
                    continue;
                }
                iter.remove();
                break;
            }
        }
        return corners;
    }

    @Override
    public String getName() {
        return "Harris Filter";
    }

    @Override
    public RGBContainer filter(RGBContainer container) {
        this.pixel = container.getRed();
        this.width = container.getWidth();
        this.height = container.getHeight();
        final List<Point> points = detect(2, 0.04);
        points.forEach(p -> drawCross(container, p));
        return container;
    }

    private void drawCross(RGBContainer container, Point p) {
        final short[] red = container.getRed();
        final short[] green = container.getGreen();
        final short[] blue = container.getBlue();
        for (int i = 0; i < 6; i++) {
            int pos1 = p.getX() + i + (p.getY() + i) * width;
            int pos2 = p.getX() + i + (p.getY() - i) * width;
            int pos3 = p.getX() - i + (p.getY() + i) * width;
            int pos4 = p.getX() - i + (p.getY() - i) * width;
            if (0 < pos1 && pos1 < red.length) {
                red[pos1] = 255;
                green[pos1] = 0;
                blue[pos1] = 255;
            }
            if (0 < pos2 && pos2 < red.length) {
                red[pos2] = 255;
                green[pos2] = 0;
                blue[pos2] = 255;
            }
            if (0 < pos3 && pos3 < red.length) {
                red[pos3] = 255;
                green[pos3] = 0;
                blue[pos3] = 255;
            }
            if (0 < pos4 && pos4 < red.length) {
                red[pos4] = 255;
                green[pos4] = 0;
                blue[pos4] = 255;
            }
        }
    }

    private static class Point {
        @Getter
        private final int x;
        @Getter
        private final int y;
        @Getter
        private float value;

        public Point(int x, int y, float h) {
            this.x = x;
            this.y = y;
            this.value = h;
        }

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }
}
