package vatbon.model;

import lombok.experimental.UtilityClass;
import vatbon.model.cells.LABCell;

@UtilityClass
public class CIEDE2000 {
    public static double diffRGB(double r1, double g1, double b1, double r2, double g2, double b2) {
        final LABCell labCell1 = new LABCell(r1, g1, b1);
        final LABCell labCell2 = new LABCell(r2, g2, b2);
        return diffLAB(labCell1.getL(), labCell1.getA(), labCell1.getB(),
                labCell2.getL(), labCell2.getA(), labCell2.getB());
    }

    public static double diffLAB(double l1, double a1, double b1, double l2, double a2, double b2) {
        double lMean = (l1 + l2) / 2.0;
        double c1 = Math.sqrt(a1 * a1 + b1 * b1);
        double c2 = Math.sqrt(a2 * a2 + b2 * b2);
        double cMean = (c1 + c2) / 2.0;

        double g = (1 - Math.sqrt(Math.pow(cMean, 7) / (Math.pow(cMean, 7) + Math.pow(25, 7)))) / 2;
        double a1prime = a1 * (1 + g);
        double a2prime = a2 * (1 + g);

        double c1Prime = Math.sqrt(a1prime * a1prime + b1 * b1);
        double c2Prime = Math.sqrt(a2prime * a2prime + b2 * b2);
        double cMeanPrime = (c1Prime + c2Prime) / 2;

        double h1prime = Math.atan2(b1, a1prime) + 2 * Math.PI * (Math.atan2(b1, a1prime) < 0 ? 1 : 0);
        double h2prime = Math.atan2(b2, a2prime) + 2 * Math.PI * (Math.atan2(b2, a2prime) < 0 ? 1 : 0);
        double hMeanPrime = ((Math.abs(h1prime - h2prime) > Math.PI) ? (h1prime + h2prime + 2 * Math.PI) / 2 : (h1prime + h2prime) / 2);

        double T = 1.0 - 0.17 * Math.cos(hMeanPrime - Math.PI / 6.0) + 0.24 * Math.cos(2 * hMeanPrime) + 0.32 * Math.cos(3 * hMeanPrime + Math.PI / 30) - 0.2 * Math.cos(4 * hMeanPrime - 21 * Math.PI / 60);

        double temp = h2prime <= h1prime ? h2prime - h1prime + 2 * Math.PI : h2prime - h1prime - 2 * Math.PI;
        double deltaHPrime = ((Math.abs(h1prime - h2prime) <= Math.PI) ? h2prime - h1prime : temp);

        double deltaLPrime = l2 - l1;
        double deltaCPrime = c2Prime - c1Prime;
        deltaHPrime = 2.0 * Math.sqrt(c1Prime * c2Prime) * Math.sin(deltaHPrime / 2.0);
        double S_L = 1.0 + ((0.015 * (lMean - 50) * (lMean - 50)) / (Math.sqrt(20 + (lMean - 50) * (lMean - 50))));
        double S_C = 1.0 + 0.045 * cMeanPrime;
        double S_H = 1.0 + 0.015 * cMeanPrime * T;

        double deltaTheta = (30 * Math.PI / 180) * Math.exp(-((180 / Math.PI * hMeanPrime - 275) / 25) * ((180 / Math.PI * hMeanPrime - 275) / 25));
        double R_C = (2 * Math.sqrt(Math.pow(cMeanPrime, 7) / (Math.pow(cMeanPrime, 7) + Math.pow(25, 7))));
        double R_T = (-R_C * Math.sin(2 * deltaTheta));

        double K_L = 1;
        double K_C = 1;
        double K_H = 1;

        return Math.sqrt(
                ((deltaLPrime / (K_L * S_L)) * (deltaLPrime / (K_L * S_L))) +
                        ((deltaCPrime / (K_C * S_C)) * (deltaCPrime / (K_C * S_C))) +
                        ((deltaHPrime / (K_H * S_H)) * (deltaHPrime / (K_H * S_H))) +
                        (R_T * (deltaCPrime / (K_C * S_C)) * (deltaHPrime / (K_H * S_H)))
        );
    }
}
