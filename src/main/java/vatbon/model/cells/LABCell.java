package vatbon.model.cells;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class LABCell {
    private float l;
    private float a;
    private float b;

    private static final float X_N = 95.04f;
    private static final float Y_N = 100.0f;
    private static final float Z_N = 108.88f;

    public static LABCell onlyL(RGBCell rgbCell) {
        LABCell labCell = new LABCell();
        final float red = rgbCell.getRed();
        final float green = rgbCell.getGreen();
        final float blue = rgbCell.getBlue();
        double y = rgb2xyzOnlyY(red / 255.0, green / 255.0, blue / 255.0);
        labCell.l = (float) (116 * f(y / Y_N) - 16);
        return labCell;
    }

    private static double rgb2xyzOnlyY(double v, double v1, double v2) {
        v = inverseCompanding(v);
        v1 = inverseCompanding(v1);
        v2 = inverseCompanding(v2);
        return 0.2126729 * v + 0.7151522 * v1 + 0.0721750 * v2;
    }

    private static class XYZContainer {
        double x;
        double y;
        double z;
    }

    public LABCell(float red, float green, float blue) {
        final XYZContainer xyzcontainer = rgb2xyz(red / 255.0, green / 255.0, blue / 255.0);
        this.l = (float) (116 * f(xyzcontainer.y / Y_N) - 16);
        this.a = (float) (500 * (f(xyzcontainer.x / X_N) - f(xyzcontainer.y / Y_N)));
        this.b = (float) (200 * (f(xyzcontainer.y / Y_N) - f(xyzcontainer.z / Z_N)));
    }

    public LABCell(double red, double green, double blue) {
        final XYZContainer xyzcontainer = rgb2xyz(red / 255.0, green / 255.0, blue / 255.0);
        this.l = (float) (116 * f(xyzcontainer.y / Y_N) - 16);
        this.a = (float) (500 * (f(xyzcontainer.x / X_N) - f(xyzcontainer.y / Y_N)));
        this.b = (float) (200 * (f(xyzcontainer.y / Y_N) - f(xyzcontainer.z / Z_N)));
    }

    public LABCell(RGBCell rgbCell) {
        final float red = rgbCell.getRed();
        final float green = rgbCell.getGreen();
        final float blue = rgbCell.getBlue();
        final XYZContainer xyzcontainer = rgb2xyz(red / 255.0, green / 255.0, blue / 255.0);
        this.l = (float) (116 * f(xyzcontainer.y / Y_N) - 16);
        this.a = (float) (500 * (f(xyzcontainer.x / X_N) - f(xyzcontainer.y / Y_N)));
        this.b = (float) (200 * (f(xyzcontainer.y / Y_N) - f(xyzcontainer.z / Z_N)));
    }

    /**
     * <a href = "http://www.brucelindbloom.com/index.html?Eqn_RGB_to_XYZ.html">algorithm</a>
     *
     * @param r red channel value from 0 to 1
     * @param g green channel value from 0 to 1
     * @param b blue channel value from 0 to 1
     * @return XYZcontainer
     */
    private XYZContainer rgb2xyz(double r, double g, double b) {
        XYZContainer xyzContainer = new XYZContainer();
        r = inverseCompanding(r);
        g = inverseCompanding(g);
        b = inverseCompanding(b);
        xyzContainer.x = (0.4124564 * r + 0.3575761 * g + 0.1804375 * b);
        xyzContainer.y = (0.2126729 * r + 0.7151522 * g + 0.0721750 * b);
        xyzContainer.z = (0.0193339 * r + 0.1191920 * g + 0.9503041 * b);
        return xyzContainer;
    }

    private static double inverseCompanding(double d) {
        double v = d;
        if (v <= 0.04045) {
            v = v / 12.92;
        } else {
            v = Math.pow(((v + 0.055) / 1.055), 2.4);
        }
        return v * 100.0;
    }

    private static double f(double x) {
        if (x > Math.pow(6.0 / 29.0, 3)) {
            return Math.cbrt(x);
        } else {
            return 1.0 / 3.0 * Math.pow(29.0 / 6.0, 2) * x + 4.0 / 29.0;
        }
    }
}
