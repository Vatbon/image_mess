package vatbon.model.cells;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RGBCell {
    private int red;
    private int green;
    private int blue;

    private static final int RED_MASK = 0xff0000;
    private static final int GREEN_MASK = 0xff00;
    private static final int BLUE_MASK = 0xff;

    public RGBCell(int argb) {
        this.red = argb >> 16 & 0xff;
        this.green = argb >> 8 & 0xff;
        this.blue = argb & 0xff;
    }

    public RGBCell(HSVCell hsvCell) {
        final float hue = hsvCell.getHue();
        final float saturation = hsvCell.getSaturation();
        final float value = hsvCell.getValue();
        double c = value * saturation;
        double x = c * (1 - Math.abs(hue / 60.0 % 2 - 1));
        double m = value - c;
        double r;
        double g;
        double b;
        if (0 <= hue && hue < 60) {
            r = c;
            g = x;
            b = 0;
        } else if (60 <= hue && hue < 120) {
            r = x;
            g = c;
            b = 0;
        } else if (120 <= hue && hue < 180) {
            r = 0;
            g = c;
            b = x;
        } else if (180 <= hue && hue < 240) {
            r = 0;
            g = x;
            b = c;
        } else if (240 <= hue && hue < 300) {
            r = x;
            g = 0;
            b = c;
        } else {
            r = c;
            g = 0;
            b = x;
        }
        this.red = (int) ((r + m) * 255);
        this.green = (int) ((g + m) * 255);
        this.blue = (int) ((b + m) * 255);
    }

    public String toString() {
        return String.format("R:%d G:%d B:%d", red, green, blue);
    }
}
