package vatbon.model.cells;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class HSVCell {
    private float hue;
    private float saturation;
    private float value;

    public HSVCell(RGBCell rgbCell) {
        final double red = rgbCell.getRed() / 255.0;
        final double blue = rgbCell.getBlue() / 255.0;
        final double green = rgbCell.getGreen() / 255.0;
        final double max = Math.max(red, Math.max(green, blue));
        final double min = Math.min(red, Math.min(green, blue));

        double delta = max - min;


        if (delta == 0) {
            hue = 0;
        } else if (max == red && green >= blue) {
            hue = (float) (60 * ((green - blue) / delta));
        } else if (max == red) {
            hue = (float) (60 * ((green - blue) / delta) + 360);
        } else if (max == green) {
            hue = (float) (60 * (blue - red) / delta + 120);
        } else {
            hue = (float) (60 * (red - green) / delta + 240);
        }
        if (max == 0.0) {
            saturation = 0;
        } else {
            saturation = (float) Math.min((1.0 - min / max), 1.0);
        }
        value = (float) max;
    }
}
