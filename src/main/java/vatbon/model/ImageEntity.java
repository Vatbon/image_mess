package vatbon.model;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import lombok.Getter;
import vatbon.model.cells.HSVCell;
import vatbon.model.cells.LABCell;
import vatbon.model.cells.RGBCell;

import java.util.ArrayList;
import java.util.List;

public class ImageEntity {
    @Getter
    private final double height;
    @Getter
    private final double width;
    @Getter
    private final Image image;
    private PixelReader pixelReader;
    private List<List<RGBCell>> rgbMatrix;
    private List<List<HSVCell>> hsvMatrix;
    private List<List<LABCell>> labMatrix;

    public ImageEntity(Image image) {
        this.width = image.getWidth();
        this.height = image.getHeight();
        this.image = image;
    }

    public List<List<RGBCell>> getRGBMatrix() {
        if (rgbMatrix == null) {
            rgbMatrix = new ArrayList<>();
            PixelReader pixelReader1 = getPixelReader();
            for (int i = 0; i < height; i++) {
                ArrayList<RGBCell> rgbCells = new ArrayList<>();
                for (int j = 0; j < width; j++) {
                    rgbCells.add(new RGBCell(pixelReader1.getArgb(j, i)));
                }
                rgbMatrix.add(rgbCells);
            }
        }
        return rgbMatrix;
    }

    public List<List<HSVCell>> getHSVMatrix() {
        hsvMatrix = new ArrayList<>();
        final List<List<RGBCell>> rgbMatrix1 = getRGBMatrix();
        for (int i = 0; i < height; i++) {
            ArrayList<HSVCell> hsvCells = new ArrayList<>();
            for (int j = 0; j < width; j++) {
                hsvCells.add(new HSVCell(rgbMatrix1.get(i).get(j)));
            }
            hsvMatrix.add(hsvCells);
        }
        return hsvMatrix;
    }

    public List<List<LABCell>> getLABMatrix() {
        labMatrix = new ArrayList<>();
        final List<List<RGBCell>> rgbMatrix1 = getRGBMatrix();
        for (int i = 0; i < height; i++) {
            ArrayList<LABCell> labCells = new ArrayList<>();
            for (int j = 0; j < width; j++) {
                labCells.add(new LABCell(rgbMatrix1.get(i).get(j)));
            }
            labMatrix.add(labCells);
        }
        return labMatrix;
    }

    private PixelReader getPixelReader() {
        if (this.pixelReader == null) {
            this.pixelReader = this.image.getPixelReader();
        }
        return this.pixelReader;
    }

    public List<List<LABCell>> getLABMatrixOnlyL() {
        labMatrix = new ArrayList<>();
        final List<List<RGBCell>> rgbMatrix1 = getRGBMatrix();
        for (int i = 0; i < height; i++) {
            ArrayList<LABCell> labCells = new ArrayList<>();
            for (int j = 0; j < width; j++) {
                labCells.add(LABCell.onlyL(rgbMatrix1.get(i).get(j)));
            }
            labMatrix.add(labCells);
        }
        return labMatrix;
    }
}
